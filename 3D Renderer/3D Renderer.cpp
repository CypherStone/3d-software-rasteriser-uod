#include "3D Renderer.h"
#include "stdafx.h"
#include <string>

LRESULT CALLBACK	MainWndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);
LRESULT CALLBACK	RenderProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);
INT_PTR CALLBACK	About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam);
BOOL				CreateNewDocument(HINSTANCE hInstance);
void				CreateUI(HWND &hWnd, LPARAM &lParam);
void				ResizeUI(HWND &hWnd);
void				LoadObject(HWND &hWnd);
void				KeyCommands(HWND &hWnd, LPARAM &lParam, WPARAM &wParam);
void				MainCommand(HWND &hWnd, LPARAM &lParam, WPARAM &wParam);
void				DrawWindow(HDC &hdc, HWND &hWnd);
void				LightingDemonstration(int time, LightingModes &newLighting);
void				DemoMode(RenderModes &newRender, LightingModes &newLighting);
void				ComputeFrameRate(HWND hWnd, DWORD &frames, DWORD &currentTime, DWORD &lastUpdate);
void				DisplayText(HDC &hdc, const int &polyCounts, const int &polysDrawn);

HINSTANCE hInst;
HWND hWndMainFrame  = NULL;
HWND hWndChildFrame = NULL;

HWND renderOptions, objectOptions, loadObject, objectComponentSB, materialText, 
	objRedEB, objRedText, objGreenEB, objGreenText, objBlueEB, 
	objBlueText, objSetMaterial, lightOptions, lightProperties, lightRedEB, 
	lightRedText, lightGreenEB, lightGreenText, lightBlueEB, lightBlueText, 
	lightSetMaterial, lightExpoEB, lightExpoText, renderSet, renderModeSB, renderModeText, 
	lightingModeSB, lightModeText, applicationDemo, objClearBtn, objectControlSB, 
	objectControlText, mouseControlSB, mouseControlText, lightComponentSB;

const char MainClassName[]  = "MainWindow";
const char ChildClassName[] = "RenderWindow";
const int StartChildrenNo = 994;

// Custom variables required for operation.
const int sideUI = 250;
const int maxFPS = 1000 / 60;
float rotateY = 0;
float rotateX = 0;
float rotateZ = 0;
float cameraX = 0;
float cameraY = 0;
float cameraZ = 0;
float cameraRotateX = 0;
float cameraRotateY = 0;
float cameraRotateZ = 0;
float lightExpo = 32.0f;
Material globalObjMaterial;
Material lightingComponents;
int modeCount = 0;
int selectedObject = 0;
int totalObjects = 0;
bool rendered = true;
bool demonstration = true;
RECT clientWindow;
RECT renderWindow;
HDC memoryBuffer;
HBITMAP memoryBitmap;
COLORREF background = RGB(55,55,55);
Mouse mouse;
MouseControl mouseControl = CAMERA;
RenderModes renderMode = WIREFRAME;
LightingModes lighting = AMBIENT;
LightSource light(lightingComponents, lightExpo, Vector3D(0.0f, 50.0f, 100.0f));
Camera camera(Vertex(0.0f, 0.0f, -10.0f, 0.0f), 0.0f, 0.0f, 0.0f, renderWindow);
DWORD frames;
DWORD currentTime;
DWORD lastUpdate;
TCHAR szFPS[32];
TCHAR renderingMode[32];
TCHAR lightingMode[32];
TCHAR lightingModel[32];

std::vector<Object3D> objects;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int show)
{
	WNDCLASSEX WndClsEx;
	MSG Msg;

	WndClsEx.cbSize			= sizeof(WNDCLASSEX);
	WndClsEx.style			= 0;
	WndClsEx.lpfnWndProc	= MainWndProc;
	WndClsEx.cbClsExtra		= 0;
	WndClsEx.cbWndExtra		= 0;
	WndClsEx.hInstance		= hInstance;
	WndClsEx.hIcon			= LoadIcon(hInstance, "3D Renderer.ico");
	WndClsEx.hCursor		= LoadCursor(NULL, IDC_ARROW);
	WndClsEx.hbrBackground  = (HBRUSH)COLOR_BACKGROUND + 1;
	WndClsEx.lpszMenuName   = MAKEINTRESOURCE(IDC_MY3DRENDERER);
	WndClsEx.lpszClassName  = MainClassName;
	WndClsEx.hIconSm		= NULL;
	
	hInst = hInstance;

	if(!RegisterClassEx(&WndClsEx))
	{
		MessageBox(NULL, "3D Renderer failed to load!", "Error", MB_OK);
		return 0;
	}

	if(!CreateNewDocument(hInstance))
	{
		return 0;
	}

	hWndMainFrame = CreateWindowEx(0L, MainClassName, "3D Renderer", WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN, CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, NULL, NULL,  hInstance, NULL);

	if(hWndMainFrame == NULL)
	{
		MessageBox(NULL, "3D Renderer failed to load!", "Error", MB_OK);
		return 0;
	}

	ShowWindow(hWndMainFrame, show);
	UpdateWindow(hWndMainFrame);

	HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDR_MAIN_MENU));

	while(GetMessage(&Msg, NULL, 0, 0) > 0)
	{
		if (!TranslateMDISysAccel(hWndChildFrame, &Msg) && !TranslateAccelerator(Msg.hwnd, hAccelTable, &Msg))
		{
			TranslateMessage(&Msg);
			DispatchMessage(&Msg);
		}
	}

	return 0;
}

BOOL CreateNewDocument(HINSTANCE hInstance)
{
	WNDCLASSEX WndClsEx;

	WndClsEx.cbSize			= sizeof(WNDCLASSEX);
	WndClsEx.style			= CS_HREDRAW | CS_VREDRAW;
	WndClsEx.lpfnWndProc	= RenderProc;
	WndClsEx.cbClsExtra		= 0;
	WndClsEx.cbWndExtra		= 0;
	WndClsEx.hInstance		= hInstance;
	WndClsEx.hIcon			= NULL;
	WndClsEx.hCursor		= LoadCursor(NULL, IDC_ARROW);
	WndClsEx.hbrBackground  = (HBRUSH)(COLOR_BTNFACE);
	WndClsEx.lpszMenuName   = NULL;
	WndClsEx.lpszClassName  = ChildClassName;
	WndClsEx.hIconSm		= NULL;

	if(!RegisterClassEx(&WndClsEx))
	{
		MessageBox(NULL,"3D Window failed to load!","Error", MB_OK);
		return FALSE;
	}

	return TRUE;
}

HWND CreateNewMDIChild(HWND hMDIClient)
{
	MDICREATESTRUCT mcs;
	HWND NewWnd;

	mcs.szTitle = "Render Area";
	mcs.szClass = ChildClassName;
	mcs.hOwner  = GetModuleHandle(NULL);
	mcs.x = mcs.cx = CW_USEDEFAULT;
	mcs.y = mcs.cy = CW_USEDEFAULT;
	mcs.style = MDIS_ALLCHILDSTYLES | WS_MAXIMIZE;;

	NewWnd = (HWND)SendMessage(hMDIClient, WM_MDICREATE, 0, (LONG)&mcs);

	if(!NewWnd)
		MessageBox(NULL,"3D Window failed to load!","Error", MB_OK);

	return NewWnd;
}

LRESULT CALLBACK MainWndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	switch(msg)
	{
		case WM_CREATE:
			{
				CreateUI(hWnd, lParam);
				CLIENTCREATESTRUCT ccs;
				GetClientRect(hWnd,&clientWindow);
				clientWindow.right -= sideUI;
				ccs.hWindowMenu  = GetSubMenu(GetMenu(hWnd), 2);
				ccs.idFirstChild = StartChildrenNo;
				hWndChildFrame = CreateWindowEx(WS_EX_CLIENTEDGE,"MDICLIENT", NULL,  WS_CHILD | WS_CLIPCHILDREN | WS_VSCROLL | WS_HSCROLL | WS_VISIBLE,
											   CW_USEDEFAULT,
											   CW_USEDEFAULT,
											   680,
											   460,
											   hWnd,
											   (HMENU)IDM_FILE_NEW,
											   GetModuleHandle(NULL),
											   (LPVOID)&ccs);

				if(hWndChildFrame == NULL)
					MessageBox(hWnd, "Could not create MDI client.", "Error", MB_OK | MB_ICONERROR);
			}
			CreateNewMDIChild(hWndChildFrame);
			break;
		case WM_SIZING:
			{
				RECT client = *((RECT *)lParam);

				if (client.right - client.left < 750)
					((RECT *)lParam)->right = client.left + 750;
				if (client.bottom - client.top < 525)
					((RECT *)lParam)->bottom = client.top + 525;
			}
			break;
		case WM_SIZE:
			{
				GetClientRect(hWnd,&clientWindow);
				clientWindow.right-= sideUI;
				HWND hWndMDI = GetDlgItem(hWnd, IDM_FILE_NEW);
				SetWindowPos(hWndMDI, NULL, 0, 0, clientWindow.right, clientWindow.bottom, SWP_NOZORDER);
				ResizeUI(hWnd);
			}
			break;
		case WM_CLOSE:
				DestroyWindow(hWnd);
			break;
		case WM_DESTROY:
			{
				SelectObject(memoryBuffer,memoryBitmap);
				DeleteObject(memoryBitmap);
				DeleteDC(memoryBuffer);
				PostQuitMessage(0);
			}
			break;
		case WM_COMMAND:
				MainCommand(hWnd, lParam, wParam);
			break;
		default:
			return DefFrameProc(hWnd, hWndChildFrame, msg, wParam, lParam);
	}
	return 0;
}

LRESULT CALLBACK RenderProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	HDC hdc;
	PAINTSTRUCT ps;
	POINT mousePoint;
	switch(msg)
	{
		case WM_SYSCOMMAND:
			// Deactivates MDI functionality as I didn't require it.
			break;
		case WM_CREATE:
			{
				hdc = GetDC(hWnd);
				GetClientRect(hWnd,&renderWindow);
				memoryBuffer = CreateCompatibleDC(NULL);
				memoryBitmap = CreateCompatibleBitmap(hdc, renderWindow.right, renderWindow.bottom);
				SelectObject(memoryBuffer, memoryBitmap);
				ReleaseDC(hWnd,hdc);

				// Intialising variables for the 3D renderer.
				frames = 0;
				currentTime = 0;
				lastUpdate = 0;
				szFPS[0] = '\0';
				renderingMode[0] = '\0';
				lightingMode[0] = '\0';
				lightingModel[0] = '\0';

				lightingComponents.ambientR = 0.0f;
				lightingComponents.ambientG = 0.0f;
				lightingComponents.ambientB = 0.0f;
				lightingComponents.diffuseR = 1.0f;
				lightingComponents.diffuseG = 1.0f;
				lightingComponents.diffuseB = 1.0f;
				lightingComponents.specularR = 0.3f;
				lightingComponents.specularG = 1.0f;
				lightingComponents.specularB = 0.3f;

				light.LightingComponents = lightingComponents;

				globalObjMaterial.ambientR = 0.0f;
				globalObjMaterial.ambientG = 0.0f;
				globalObjMaterial.ambientB = 0.0f;
				globalObjMaterial.diffuseR = 1.0f;
				globalObjMaterial.diffuseG = 0.4f;
				globalObjMaterial.diffuseB = 0.4f;
				globalObjMaterial.specularR = 1.0f;
				globalObjMaterial.specularG = 1.0f;
				globalObjMaterial.specularB = 1.0f;

				SetTimer(hWnd, IDT_FRAMES, maxFPS, (TIMERPROC) NULL);
			}
			break;
		case WM_PAINT:
			{
				hdc = BeginPaint(hWnd, &ps);
				HBRUSH brush = CreateSolidBrush(background);
				HGDIOBJ oldBrush = SelectObject(memoryBuffer, brush);
				BitBlt(memoryBuffer, 0, 0, renderWindow.right, renderWindow.bottom, memoryBuffer, 0, 0, PATCOPY);
				SelectObject(memoryBuffer, oldBrush);
				DeleteObject(brush);	
				DeleteObject(oldBrush);
				DrawWindow(memoryBuffer, hWnd);
				BitBlt(hdc, 0, 0, renderWindow.right, renderWindow.bottom, memoryBuffer,0,0,SRCCOPY);
				EndPaint(hWnd, &ps);
				ComputeFrameRate(hWnd, frames, currentTime, lastUpdate); 
			}
			break;
		case WM_KEYDOWN:
				KeyCommands(hWnd, lParam, wParam);
			break;
		case WM_TIMER:
			{
				if(wParam == IDT_FRAMES)
				{
					if(rendered)
					{
						rendered = false;
						InvalidateRect(hWnd, NULL, FALSE);
						SetTimer(hWnd, IDT_FRAMES, maxFPS, (TIMERPROC) NULL);
						return 0;
					}
				}
			}
			break;
		case WM_MOUSEWHEEL:
		{
			short wheel = (short) HIWORD(wParam) / 120;
			if (wheel > 0 && !mouse.GetRClick())
				cameraZ = -1.0f;
			else if (wheel < 0 && !mouse.GetRClick())
				cameraZ = 1.0f;	

			if (wheel > 0 && mouse.GetRClick())
				cameraRotateZ = -0.1f;
			else if (wheel < 0 && mouse.GetRClick())
				cameraRotateZ = 0.1f;
		}
		case WM_MOUSEMOVE:
			{
				GetCursorPos(&mousePoint);
				ScreenToClient(hWnd, &mousePoint);
				mouse.SetX((float)mousePoint.x);
				mouse.SetY((float)mousePoint.y);

				if(!mouse.GetLClick() && !mouse.GetRClick())
					break;

				if(mouse.GetLClick())
				{
					cameraX = (mouse.GetMemX() - mouse.GetX()) * 0.1f;
					cameraY = (mouse.GetY() - mouse.GetMemY()) * 0.1f;
				}
				else if(mouse.GetRClick())
				{
					cameraRotateX = (mouse.GetX() - mouse.GetMemX()) * 0.01f;
					cameraRotateY = (mouse.GetMemY() - mouse.GetY()) * 0.01f;
				}

				mouse.SetMemX((float)mousePoint.x);
				mouse.SetMemY((float)mousePoint.y);
			}
			break;
		case WM_LBUTTONDOWN:
			{
				if(!mouse.GetLClick())
					SetCapture(hWnd);

				GetCursorPos(&mousePoint);
				ScreenToClient(hWnd, &mousePoint);
				mouse.SetMemX((float)mousePoint.x);
				mouse.SetMemY((float)mousePoint.y);
				mouse.SetLClick(true);
			}
			break;
		case WM_LBUTTONUP:
			{
				if(mouse.GetLClick())
					ReleaseCapture();

				mouse.SetLClick(false);
			}
			break;
		case WM_RBUTTONDOWN:	
			{
				if(!mouse.GetRClick())
					SetCapture(hWnd);
	
				GetCursorPos(&mousePoint);
				ScreenToClient(hWnd, &mousePoint);
				mouse.SetMemX((float)mousePoint.x);
				mouse.SetMemY((float)mousePoint.y);
				mouse.SetRClick(true);
			}
			break;
		case WM_RBUTTONUP:
				if(mouse.GetRClick())
					ReleaseCapture();
				
				mouse.SetRClick(false); 
			break;
		case WM_ERASEBKGND:
			break;
		case WM_SIZE:
			{
				hdc = GetDC(hWnd);
				GetClientRect(hWnd, &renderWindow);
				memoryBitmap = CreateCompatibleBitmap(hdc, renderWindow.right, renderWindow.bottom);
				SelectObject(memoryBuffer, memoryBitmap);
				ReleaseDC(hWnd,hdc);
				camera.SetScreen(renderWindow);

			}
			return DefMDIChildProc(hWnd, msg, wParam, lParam);
		default:
			return DefMDIChildProc(hWnd, msg, wParam, lParam);
	}
	
	return 0;
}
 
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
		{
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		break;
	}
	return (INT_PTR)FALSE;
}

void CreateUI(HWND &hWnd, LPARAM &lParam)
{
	// This a function that creates all of the GUI and intialises the appropriate data.
	renderOptions = CreateWindowW(L"button", L"Render Properties",	WS_VISIBLE | WS_CHILD | BS_GROUPBOX, 0, 0, 0, 0, hWnd, (HMENU)IDC_TOOLBOX, NULL, NULL);	
	renderModeSB = CreateWindowW(L"combobox", NULL, WS_VISIBLE | WS_CHILD | CBS_DROPDOWNLIST, 0, 0, 0, 0, hWnd, (HMENU)IDC_RENDERMODESB, NULL, NULL);
	renderModeText = CreateWindowW(L"STATIC", L"Render", WS_VISIBLE | WS_CHILD, 0, 0, 0, 0, hWnd, (HMENU)1, NULL, NULL);
	lightingModeSB = CreateWindowW(L"combobox", NULL, WS_VISIBLE | WS_CHILD | CBS_DROPDOWNLIST, 0, 0, 0, 0, hWnd, (HMENU)IDC_LIGHTMODESB, NULL, NULL);
	lightModeText = CreateWindowW(L"STATIC", L"Lighting", WS_VISIBLE | WS_CHILD, 0, 0, 0, 0, hWnd, (HMENU)1, NULL, NULL);
	mouseControlSB = CreateWindowW(L"combobox", NULL, WS_VISIBLE | WS_CHILD | CBS_DROPDOWNLIST, 0, 0, 0, 0, hWnd, (HMENU)IDC_MOUSECONTROL, NULL, NULL);
	mouseControlText = CreateWindowW(L"STATIC", L"Mouse Control", WS_VISIBLE | WS_CHILD, 0, 0, 0, 0, hWnd, (HMENU)1, NULL, NULL);
	applicationDemo = CreateWindowW(L"button", L"Demonstration Mode", WS_VISIBLE | WS_CHILD | BS_AUTOCHECKBOX, 0, 0, 0, 0, hWnd, (HMENU)IDC_DEMOMODECB, NULL, NULL);
	renderSet = CreateWindowW(L"button", L"Set Properties", WS_VISIBLE | WS_CHILD, 0, 0, 0, 0, hWnd, (HMENU)IDC_RENDERSET, NULL, NULL);

	objectOptions = CreateWindowW(L"button", L"Objects Properties",	WS_VISIBLE | WS_CHILD |BS_GROUPBOX, 0, 0, 0, 0, hWnd, (HMENU)IDC_TOOLBOX, NULL, NULL);
	loadObject = CreateWindowW(L"button", L"Load Object", WS_VISIBLE | WS_CHILD, 0, 0, 0, 0, hWnd, (HMENU)IDC_LOADOBJECT, NULL, NULL);
	objClearBtn = CreateWindowW(L"button", L"Clear Objects", WS_VISIBLE | WS_CHILD, 0, 0, 0, 0, hWnd, (HMENU)IDC_CLEAROBJECT, NULL, NULL);
	objectComponentSB = CreateWindowW(L"combobox", NULL, WS_VISIBLE | WS_CHILD | CBS_DROPDOWNLIST, 0, 0, 0, 0, hWnd, (HMENU)IDC_COLOUROBJECT, NULL, NULL);
	materialText = CreateWindowW(L"STATIC", L"Components",	WS_VISIBLE | WS_CHILD, 0, 0, 0, 0, hWnd, (HMENU) 1, NULL, NULL);
	objRedEB = CreateWindowW(L"Edit", L"5",	WS_VISIBLE | WS_CHILD | WS_BORDER | ES_NUMBER | ES_CENTER, 0, 0, 0, 0, hWnd, (HMENU) IDC_OBJAMBTEXT, NULL, NULL);
	objRedText  = CreateWindowW(L"STATIC", L"Red", WS_VISIBLE | WS_CHILD, 0, 0, 0, 0, hWnd, (HMENU) 1, NULL, NULL);
	objGreenEB = CreateWindowW(L"Edit", L"5",	WS_VISIBLE | WS_CHILD | WS_BORDER | ES_NUMBER | ES_CENTER, 0, 0, 0, 0, hWnd, (HMENU) IDC_OBJDIFTEXT, NULL, NULL);
	objGreenText  = CreateWindowW(L"STATIC", L"Green",	WS_VISIBLE | WS_CHILD, 0, 0, 0, 0, hWnd, (HMENU) 1, NULL, NULL);
	objBlueEB = CreateWindowW(L"Edit", L"5", WS_VISIBLE | WS_CHILD | WS_BORDER | ES_NUMBER | ES_CENTER, 0, 0, 0, 0, hWnd, (HMENU) IDC_OBJSPECTEXT, NULL, NULL);
	objBlueText  = CreateWindowW(L"STATIC", L"Blue", WS_VISIBLE | WS_CHILD, 0, 0, 0, 0, hWnd, (HMENU) 1, NULL, NULL);
	objSetMaterial = CreateWindowW(L"button", L"Set Properties", WS_VISIBLE | WS_CHILD, 0, 0, 0, 0, hWnd, (HMENU)IDC_OBJSETMAT, NULL, NULL);
	objectControlSB = CreateWindowW(L"combobox", NULL, WS_VISIBLE | WS_CHILD | CBS_DROPDOWNLIST, 0, 0, 0, 0, hWnd, (HMENU)IDC_OBJECTCONTROL, NULL, NULL);
	objectControlText = CreateWindowW(L"STATIC", L"Select Object",	WS_VISIBLE | WS_CHILD, 0, 0, 0, 0, hWnd, (HMENU) 1, NULL, NULL);

	lightOptions = CreateWindowW(L"button", L"Light Properties",	WS_VISIBLE | WS_CHILD |BS_GROUPBOX, 0, 0, 0, 0, hWnd, (HMENU)IDC_TOOLBOX, NULL, NULL);
	lightProperties = CreateWindowW(L"STATIC", L"Components",	WS_VISIBLE | WS_CHILD, 0, 0, 0, 0, hWnd, (HMENU) 1, NULL, NULL);
	lightComponentSB = CreateWindowW(L"combobox", NULL, WS_VISIBLE | WS_CHILD | CBS_DROPDOWNLIST, 0, 0, 0, 0, hWnd, (HMENU)IDC_LIGHTCOMPONENTSB, NULL, NULL);
	lightRedEB = CreateWindowW(L"Edit", L"5",	WS_VISIBLE | WS_CHILD | WS_BORDER | ES_NUMBER | ES_CENTER, 0, 0, 0, 0, hWnd, (HMENU) IDC_LIGHTAMBTEXT, NULL, NULL);
	lightRedText  = CreateWindowW(L"STATIC", L"Red", WS_VISIBLE | WS_CHILD, 0, 0, 0, 0, hWnd, (HMENU) 1, NULL, NULL);
	lightGreenEB = CreateWindowW(L"Edit", L"5",	WS_VISIBLE | WS_CHILD | WS_BORDER | ES_NUMBER | ES_CENTER, 0, 0, 0, 0, hWnd, (HMENU) IDC_LIGHTDIFTEXT, NULL, NULL);
	lightGreenText  = CreateWindowW(L"STATIC", L"Green",	WS_VISIBLE | WS_CHILD, 0, 0, 0, 0, hWnd, (HMENU) 1, NULL, NULL);
	lightBlueEB = CreateWindowW(L"Edit", L"5", WS_VISIBLE | WS_CHILD | WS_BORDER | ES_NUMBER | ES_CENTER, 0, 0, 0, 0, hWnd, (HMENU) IDC_LIGHTSPECTEXT, NULL, NULL);
	lightBlueText  = CreateWindowW(L"STATIC", L"Blue", WS_VISIBLE | WS_CHILD, 0, 0, 0, 0, hWnd, (HMENU) 1, NULL, NULL);
	lightExpoEB = CreateWindowW(L"Edit", L"32", WS_VISIBLE | WS_CHILD | WS_BORDER | ES_NUMBER | ES_CENTER, 0, 0, 0, 0, hWnd, (HMENU) IDC_LIGHTEXPOTEXT, NULL, NULL);
	lightExpoText  = CreateWindowW(L"STATIC", L"Specular Exponent", WS_VISIBLE | WS_CHILD, 0, 0, 0, 0, hWnd, (HMENU) 1, NULL, NULL);
	lightSetMaterial = CreateWindowW(L"button", L"Set Properties", WS_VISIBLE | WS_CHILD, 0, 0, 0, 0, hWnd, (HMENU)IDC_LIGHTSETMAT, NULL, NULL);

	// Making all of the text the same defualt font.
	HFONT defaultFont;
	defaultFont = (HFONT)GetStockObject(DEFAULT_GUI_FONT);
	SendMessage(renderOptions, WM_SETFONT, WPARAM (defaultFont), TRUE);
	SendMessage(renderModeSB, WM_SETFONT, WPARAM (defaultFont), TRUE);
	SendMessage(renderModeText, WM_SETFONT, WPARAM (defaultFont), TRUE);
	SendMessage(lightingModeSB, WM_SETFONT, WPARAM (defaultFont), TRUE);
	SendMessage(lightModeText, WM_SETFONT, WPARAM (defaultFont), TRUE);
	SendMessage(mouseControlSB, WM_SETFONT, WPARAM (defaultFont), TRUE);
	SendMessage(mouseControlText, WM_SETFONT, WPARAM (defaultFont), TRUE);
	SendMessage(applicationDemo, WM_SETFONT, WPARAM (defaultFont), TRUE);
	SendMessage(renderSet, WM_SETFONT, WPARAM (defaultFont), TRUE);
	SendMessage(objectOptions, WM_SETFONT, WPARAM (defaultFont), TRUE);
	SendMessage(objClearBtn, WM_SETFONT, WPARAM (defaultFont), TRUE);
	SendMessage(loadObject, WM_SETFONT, WPARAM (defaultFont), TRUE);
	SendMessage(objectComponentSB, WM_SETFONT, WPARAM (defaultFont), TRUE);
	SendMessage(materialText, WM_SETFONT, WPARAM (defaultFont), TRUE);
	SendMessage(objRedEB, WM_SETFONT, WPARAM (defaultFont), TRUE);
	SendMessage(objRedText, WM_SETFONT, WPARAM (defaultFont), TRUE);
	SendMessage(objGreenEB, WM_SETFONT, WPARAM (defaultFont), TRUE);
	SendMessage(objGreenText, WM_SETFONT, WPARAM (defaultFont), TRUE);
	SendMessage(objBlueEB, WM_SETFONT, WPARAM (defaultFont), TRUE);
	SendMessage(objBlueText, WM_SETFONT, WPARAM (defaultFont), TRUE);
	SendMessage(objSetMaterial, WM_SETFONT, WPARAM (defaultFont), TRUE);
	SendMessage(objectControlSB, WM_SETFONT, WPARAM (defaultFont), TRUE);	
	SendMessage(objectControlText, WM_SETFONT, WPARAM (defaultFont), TRUE);
	SendMessage(lightOptions, WM_SETFONT, WPARAM (defaultFont), TRUE);
	SendMessage(lightComponentSB, WM_SETFONT, WPARAM (defaultFont), TRUE);
	SendMessage(lightProperties, WM_SETFONT, WPARAM (defaultFont), TRUE);
	SendMessage(lightRedEB, WM_SETFONT, WPARAM (defaultFont), TRUE);
	SendMessage(lightRedText, WM_SETFONT, WPARAM (defaultFont), TRUE);
	SendMessage(lightGreenEB, WM_SETFONT, WPARAM (defaultFont), TRUE);	
	SendMessage(lightGreenText, WM_SETFONT, WPARAM (defaultFont), TRUE);
	SendMessage(lightBlueEB, WM_SETFONT, WPARAM (defaultFont), TRUE);
	SendMessage(lightBlueText, WM_SETFONT, WPARAM (defaultFont), TRUE);
	SendMessage(lightExpoEB, WM_SETFONT, WPARAM (defaultFont), TRUE);
	SendMessage(lightExpoText, WM_SETFONT, WPARAM (defaultFont), TRUE);
	SendMessage(lightSetMaterial, WM_SETFONT, WPARAM (defaultFont), TRUE);
	DeleteObject(defaultFont);


	SendMessage(renderModeSB, CB_ADDSTRING, 0, (LPARAM)TEXT("Wireframe"));
	SendMessage(renderModeSB, CB_ADDSTRING, 0, (LPARAM)TEXT("Polygon"));
	SendMessage(renderModeSB, CB_ADDSTRING, 0, (LPARAM)TEXT("Vertex"));
	SendMessage(renderModeSB, CB_ADDSTRING, 0, (LPARAM)TEXT("Textured"));
	SendMessage(renderModeSB, CB_SETCURSEL, 0, 0);

	SendMessage(objectComponentSB, CB_ADDSTRING, 0, (LPARAM)TEXT("Ambient"));
	SendMessage(objectComponentSB, CB_ADDSTRING, 0, (LPARAM)TEXT("Diffuse"));
	SendMessage(objectComponentSB, CB_ADDSTRING, 0, (LPARAM)TEXT("Specular"));
	SendMessage(objectComponentSB, CB_SETCURSEL, 0, 0);

	SendMessage(lightingModeSB, CB_ADDSTRING, 0, (LPARAM)TEXT("Ambient"));
	SendMessage(lightingModeSB, CB_ADDSTRING, 0, (LPARAM)TEXT("Diffuse"));
	SendMessage(lightingModeSB, CB_ADDSTRING, 0, (LPARAM)TEXT("Specular"));
	SendMessage(lightingModeSB, CB_SETCURSEL, 0, 0);

	SendMessage(lightComponentSB, CB_ADDSTRING, 0, (LPARAM)TEXT("Ambient"));
	SendMessage(lightComponentSB, CB_ADDSTRING, 0, (LPARAM)TEXT("Diffuse"));
	SendMessage(lightComponentSB, CB_ADDSTRING, 0, (LPARAM)TEXT("Specular"));
	SendMessage(lightComponentSB, CB_SETCURSEL, 0, 0);

	SendMessage(mouseControlSB, CB_ADDSTRING, 0, (LPARAM)TEXT("Camera"));
	SendMessage(mouseControlSB, CB_ADDSTRING, 0, (LPARAM)TEXT("Objects"));
	SendMessage(mouseControlSB, CB_ADDSTRING, 0, (LPARAM)TEXT("Light"));
	SendMessage(mouseControlSB, CB_SETCURSEL, 0, 0);

	CheckDlgButton(hWnd, IDC_DEMOMODECB, BST_CHECKED);
}

void ResizeUI(HWND &hWnd)
{
	// A function that handles all of the UI scaling and position.
	const int groupAdjust = clientWindow.right+10;
	const int centerObjs = clientWindow.right + 80;
	const int leftObjs = clientWindow.right + 25;
	const int midObjs = clientWindow.right + 105;
	const int midSB = clientWindow.right + 125;
	const int rightObjs = clientWindow.right + 170;

	ShowWindow(renderOptions, SW_HIDE); 
	MoveWindow(renderOptions, groupAdjust, 10, 230, 220, FALSE);
	MoveWindow(renderModeSB, midSB, 30, 100, 100, FALSE);
	MoveWindow(renderModeText, leftObjs, 32, 90, 25, FALSE);
	MoveWindow(lightingModeSB, midSB, 60, 100, 100, FALSE);
	MoveWindow(lightModeText, leftObjs, 62, 90, 25, FALSE);
	MoveWindow(applicationDemo, leftObjs + 35, 120, 160, 25, FALSE);
	MoveWindow(mouseControlSB, midSB, 90, 100, 100, FALSE);
	MoveWindow(mouseControlText, leftObjs, 92, 95, 25, FALSE);
	MoveWindow(renderSet, centerObjs, 190, 100, 25, FALSE);
	ShowWindow(renderOptions, SW_SHOW); 

	ShowWindow(objectOptions, SW_HIDE);
	MoveWindow(objectOptions, groupAdjust, 240, 230, 220, FALSE);
	MoveWindow(loadObject, leftObjs - 5, 260, 100, 25, FALSE);
	MoveWindow(objClearBtn, midSB, 260, 100, 25, FALSE);
	MoveWindow(objectComponentSB, midSB, 290, 100, 100, FALSE);
	MoveWindow(materialText, leftObjs, 292, 100, 25, FALSE);
	MoveWindow(objRedEB, leftObjs + 5, 320, 50, 25, FALSE);
	MoveWindow(objRedText,leftObjs + 5, 345, 100, 25, FALSE);
	MoveWindow(objGreenEB, midObjs - 5, 320, 50, 25, FALSE);
	MoveWindow(objGreenText, midObjs - 5, 345, 100, 25, FALSE);
	MoveWindow(objBlueEB, rightObjs + 5, 320, 50, 25, FALSE);
	MoveWindow(objBlueText, rightObjs + 5, 345, 60, 25, FALSE);
	MoveWindow(objectControlText, leftObjs, 367, 90, 25, FALSE);
	MoveWindow(objectControlSB, midSB, 365, 100, 100, FALSE);
	MoveWindow(objSetMaterial, centerObjs, 420, 100, 25, FALSE);
	ShowWindow(objectOptions, SW_SHOW); 

	ShowWindow(lightOptions, SW_HIDE);
	MoveWindow(lightOptions, groupAdjust, 470, 230, 220, FALSE);
	MoveWindow(lightProperties, leftObjs, 492, 100, 25, FALSE);
	MoveWindow(lightComponentSB, midSB, 490, 100, 100, FALSE);
	MoveWindow(lightRedEB, leftObjs, 520, 50, 25, FALSE);
	MoveWindow(lightRedText, leftObjs, 550, 100, 25, FALSE);
	MoveWindow(lightGreenEB, midObjs - 5, 520, 50, 25, FALSE);
	MoveWindow(lightGreenText, midObjs - 5, 550, 100, 25, FALSE);
	MoveWindow(lightBlueEB, rightObjs + 5, 520, 50, 25, FALSE);
	MoveWindow(lightBlueText, rightObjs + 5, 550, 60, 25, FALSE);
	MoveWindow(lightExpoEB, midObjs - 5, 570, 50, 25, FALSE);
	MoveWindow(lightExpoText, midObjs - 35, 600, 125, 25, FALSE);
	MoveWindow(lightSetMaterial, centerObjs, 650, 100, 25, FALSE);
	ShowWindow(lightOptions, SW_SHOW); 
}

void LoadObject(HWND &hWnd)
{
	// Loads an object from an OBJ file.
	Object3D obj;
	char fileString[MAX_PATH] = "";
	if(Utility::FileDialog(hWnd, fileString, "Load 3D Object"))
	{
		Utility::ObjFromFile(obj, fileString);

		// Setting the objects material to the global object material of the application.
		obj.ObjMaterial = globalObjMaterial;

		// Adds the object to a vector of Object3D.
		objects.push_back(obj);

		totalObjects++;
		TCHAR objects[100];
		wsprintf(objects, _T("Object %d"), totalObjects);
		SendMessage(objectControlSB, CB_ADDSTRING, 0, (LPARAM)TEXT(objects));
		SendMessage(objectControlSB, CB_SETCURSEL, totalObjects - 1, 0);

		if(totalObjects > 1)
			selectedObject++;
	}
}

void KeyCommands(HWND &hWnd, LPARAM &lParam, WPARAM &wParam)
{
	// Processing any keyboard input from the user.
	if(wParam == 0x5A) 
	{
		//Z Key
		rotateY = -0.1f;
	}
	else if(wParam == 0x58) 
	{
		//X Key
		rotateY = 0.1f;
	}
	else if(wParam == 'A') 
	{
		//A Key
		rotateX = -0.1f;
	}
	else if(wParam == 0x53) 
	{
		//S Key 
		rotateX = 0.1f;
	}
	else if(wParam == 0x4C) 
	{
		//L Key - Load Function
		LoadObject(hWnd);
	}
	
}

void MainCommand(HWND &hWnd, LPARAM &lParam, WPARAM &wParam) 
{
	// This function controls input from the user.
	switch(LOWORD(wParam))
	{
		case ID_HELP_ABOUT:
				DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
			break;
		case IDM_FILE_EXIT:
				PostMessage(hWnd, WM_CLOSE, 0, 0);
			break;
		case IDC_LOADOBJECT:
			{
				LoadObject(hWnd);
				SetFocus(hWnd);
			}
			break;
		case IDC_CLEAROBJECT:
			{
				totalObjects = 0;
				selectedObject = 0;
				SendMessage(objectControlSB, CB_RESETCONTENT, 0, 0);
				objects.clear();
				SetFocus(hWnd);
			}
			break;
		case IDC_OBJSETMAT:
			{
				if(totalObjects > 0)
				{
					selectedObject = (int)SendMessage(objectControlSB, CB_GETCURSEL, 0, 0);
					LightingModes seletedLight = (LightingModes)SendMessage(objectComponentSB, CB_GETCURSEL, 0, 0);

					if(seletedLight == AMBIENT)
					{
						globalObjMaterial.ambientR = GetDlgItemInt(hWnd, IDC_OBJAMBTEXT, NULL, FALSE) * 0.1f;
						globalObjMaterial.ambientG = GetDlgItemInt(hWnd, IDC_OBJDIFTEXT, NULL, FALSE) * 0.1f;
						globalObjMaterial.ambientB = GetDlgItemInt(hWnd, IDC_OBJSPECTEXT, NULL, FALSE) * 0.1f;
					}
					else if(seletedLight == DIFFUSE)
					{
						globalObjMaterial.diffuseR = GetDlgItemInt(hWnd, IDC_OBJAMBTEXT, NULL, FALSE) * 0.1f;
						globalObjMaterial.diffuseG = GetDlgItemInt(hWnd, IDC_OBJDIFTEXT, NULL, FALSE) * 0.1f;
						globalObjMaterial.diffuseB = GetDlgItemInt(hWnd, IDC_OBJSPECTEXT, NULL, FALSE) * 0.1f;
					}
					else if(seletedLight == SPECULAR)
					{
						globalObjMaterial.specularR = GetDlgItemInt(hWnd, IDC_OBJAMBTEXT, NULL, FALSE) * 0.1f;
						globalObjMaterial.specularG = GetDlgItemInt(hWnd, IDC_OBJDIFTEXT, NULL, FALSE) * 0.1f;
						globalObjMaterial.specularB = GetDlgItemInt(hWnd, IDC_OBJSPECTEXT, NULL, FALSE) * 0.1f;
					}

					objects[selectedObject].ObjMaterial = globalObjMaterial;
				}
				SetFocus(hWnd);
			}
			break;
		case IDC_LIGHTSETMAT:
			{
				LightingModes seletedLight = (LightingModes)SendMessage(lightComponentSB, CB_GETCURSEL, 0, 0);

				if(seletedLight == AMBIENT)
				{
					lightingComponents.ambientR = GetDlgItemInt(hWnd, IDC_LIGHTAMBTEXT, NULL, FALSE) * 0.1f;
					lightingComponents.ambientG = GetDlgItemInt(hWnd, IDC_LIGHTDIFTEXT, NULL, FALSE) * 0.1f;
					lightingComponents.ambientB = GetDlgItemInt(hWnd, IDC_LIGHTSPECTEXT, NULL, FALSE) * 0.1f;
				}
				else if(seletedLight == DIFFUSE)
				{
					lightingComponents.diffuseR = GetDlgItemInt(hWnd, IDC_LIGHTAMBTEXT, NULL, FALSE) * 0.1f;
					lightingComponents.diffuseG = GetDlgItemInt(hWnd, IDC_LIGHTDIFTEXT, NULL, FALSE) * 0.1f;
					lightingComponents.diffuseB = GetDlgItemInt(hWnd, IDC_LIGHTSPECTEXT, NULL, FALSE) * 0.1f;
				}
				else if(seletedLight == SPECULAR)
				{
					lightingComponents.specularR = GetDlgItemInt(hWnd, IDC_LIGHTAMBTEXT, NULL, FALSE) * 0.1f;
					lightingComponents.specularG = GetDlgItemInt(hWnd, IDC_LIGHTDIFTEXT, NULL, FALSE) * 0.1f;
					lightingComponents.specularB = GetDlgItemInt(hWnd, IDC_LIGHTSPECTEXT, NULL, FALSE) * 0.1f;
				}

				lightExpo =  (float)GetDlgItemInt(hWnd, IDC_LIGHTEXPOTEXT, NULL, FALSE);
				light.LightingComponents = lightingComponents;
				light.SpecularExponent(lightExpo);
				SetFocus(hWnd);
			}
			break;
		case IDC_RENDERSET:
			{
				int modeSelect = (int)SendMessage(renderModeSB, CB_GETCURSEL, 0, 0);
				int lightSelect = (int)SendMessage(lightingModeSB, CB_GETCURSEL, 0, 0);
				int mouseSelect = (int)SendMessage(mouseControlSB, CB_GETCURSEL, 0, 0);

				renderMode = (RenderModes)modeSelect;
				lighting = (LightingModes)lightSelect;
				mouseControl = (MouseControl)mouseSelect;

				if(SendMessage(applicationDemo, BM_GETSTATE, 0, 0) == BST_CHECKED)
					demonstration = true;
				else
					demonstration = false;

				SetFocus(hWnd);
			}
			break;
		default:
		{
			if(LOWORD(wParam) >= StartChildrenNo)
			{
				DefFrameProc(hWnd, hWndChildFrame, WM_COMMAND, wParam, lParam);
			}
			else 
			{
				HWND hWndCurrent = (HWND)SendMessage(hWndChildFrame, WM_MDIGETACTIVE,0,0);
				if(hWndCurrent)
				{
					SendMessage(hWndCurrent, WM_COMMAND, wParam, lParam);
				}
			}
		}
	}
}

void LightingDemonstration(int time, LightingModes &newLighting)
{
	// Iterates through the 3 stages of lighting.
	if(modeCount < time)
	{
		newLighting = AMBIENT;
		wsprintf(lightingMode, _T("Lighting: Ambient"));
	}
	else if(modeCount < time + 5)
	{
		newLighting = DIFFUSE;
		wsprintf(lightingMode, _T("Lighting: Diffuse"));
	}
	else
	{
		newLighting = SPECULAR;
		wsprintf(lightingMode, _T("Lighting: Specular"));
	}
}

void DemoMode(RenderModes &newRender, LightingModes &newLighting)
{
	// Iterates through the stages of shading.
	if(modeCount >= 5 && modeCount < 20)
	{
		newRender = PERPOLYSHADE;
		LightingDemonstration(10, newLighting);
	}
	else if(modeCount >= 20 && modeCount < 40)
	{
		newRender = PERVERTEXSHADE;
		LightingDemonstration(25, newLighting);
	}
	else if(modeCount >= 40)
	{
		newRender = TEXTURED;
	}
	else if(modeCount < 5)
	{
		newRender = WIREFRAME;
	}
}

void DrawWindow(HDC &hdc, HWND &hWnd)
{
	// Creates a copy of the rendering and lighting modes, so that they can be changed if needed too.
	RenderModes newRenderMode = renderMode;
	LightingModes newLighting = lighting;

	float idleRotate = 0.0f;

	if(demonstration)
	{
		DemoMode(newRenderMode, newLighting);
		idleRotate = 0.1f;
	}

	// Update the text in the User Interface.
	if(newLighting == AMBIENT)
		wsprintf(lightingMode, _T("Lighting: Ambient"));
	else if(newLighting == DIFFUSE)
		wsprintf(lightingMode, _T("Lighting: Diffuse"));
	else if(newLighting == SPECULAR)
		wsprintf(lightingMode, _T("Lighting: Specular"));

	if(newRenderMode == WIREFRAME)
	{
		wsprintf(renderingMode, _T("Render: Wireframe"));
		wsprintf(lightingMode, _T("Lighting: None"));
	}
	else if(newRenderMode == PERPOLYSHADE)
		wsprintf(renderingMode, _T("Render: Polygon Shading"));
	else if(newRenderMode == PERVERTEXSHADE)
		wsprintf(renderingMode, _T("Render: Vertex Shading"));
	else if(newRenderMode == TEXTURED)
	{
		wsprintf(lightingMode, _T("Lighting: None"));
		wsprintf(renderingMode, _T("Render: Textured"));
	}

	if(!objects.empty())
	{
		// This section of the code will adjust the scene according to the user input.
		float modelRotateX = 0.0f, modelRotateY = 0.0f, modelRotateZ = 0.0f;
		float modelX = 0.0f, modelY = 0.0f, modelZ = 0.0f;

		if(mouseControl == OBJECT)
		{
			modelRotateX = cameraRotateY; modelRotateY = cameraRotateX; modelRotateZ = cameraRotateZ; 
			modelX = -cameraX; modelY = cameraY; modelZ = cameraZ; 
		}
		else if(mouseControl == LIGHT)
			light.Position(Vector3D(light.Position().x() + cameraX, light.Position().y() + cameraY, light.Position().z() + cameraZ));
		else if(mouseControl == CAMERA)
		{
			camera.SetPosition(Vertex(camera.GetPosition().x() + cameraX, camera.GetPosition().y() + cameraY, camera.GetPosition().z() + cameraZ, 1));
			camera.SetXAngle(camera.GetXAngle() + -cameraRotateY); camera.SetYAngle(camera.GetYAngle() + cameraRotateX); camera.SetZAngle(camera.GetZAngle() + cameraRotateZ); 
		}

		objects[selectedObject].XAngle(objects[selectedObject].XAngle() + -modelRotateX + rotateX);
		objects[selectedObject].YAngle(objects[selectedObject].YAngle() + modelRotateY + rotateY + idleRotate);
		objects[selectedObject].ZAngle(objects[selectedObject].ZAngle() + modelRotateZ + rotateZ);
		objects[selectedObject].Position(Vector3D(objects[selectedObject].Position().x() + modelX, objects[selectedObject].Position().y() + modelY, objects[selectedObject].Position().z() + modelZ));

		// Sends all of the objects to get rendered.
		RenderEngine render(hdc, objects, camera, light, newRenderMode, newLighting);
		
		DisplayText(hdc, render.GetTotalPolygons(), render.GetRenderedPolygons());
	}
	else
	{
		modeCount = 0;
		DisplayText(hdc, 0, 0);
	}

	// Resets all of the variables for the next render.
	rotateX = 0.0f, rotateY = 0.0f, rotateZ = 0.0f, 
	cameraRotateX = 0.0f, cameraRotateY = 0.0f, cameraRotateZ = 0.0f, 
	cameraX = 0.0f, cameraY  = 0.0f, cameraZ = 0.0f;
}

void ComputeFrameRate(HWND hWnd, DWORD &frames, DWORD &currentTime, DWORD &lastUpdate)
{
	// A simple mechanism to compute frame rate.
	frames++;
	currentTime = GetTickCount(); 
	DWORD elapsedTime = currentTime - lastUpdate;

	if(elapsedTime >= 1000)
	{
		lastUpdate = currentTime;
		wsprintf(szFPS, _T("FPS: %u"), (UINT)(frames * 1000.0 / elapsedTime));	
		
		if(demonstration)
		{
			// Resets the demo mode after a minute.
			modeCount++;
			if(modeCount > 60)
				modeCount = 0;
		}
		frames = 0;
	}
	rendered = true;
}

void DisplayText(HDC &hdc, const int &polyCounts, const int &polysDrawn)
{
	// All of the UI text.
	SetBkMode(hdc, TRANSPARENT);
	SetTextColor(hdc, RGB(255,255,255));
	HFONT hFont = (HFONT)GetStockObject(DEFAULT_GUI_FONT);
	HFONT hTmp = (HFONT)SelectObject(hdc, hFont);

	TCHAR polyTotalDisplay[50];
	TCHAR namePrintDisplay[50];
	TCHAR polyVisibleDisplay[50];
	TCHAR mouseControlDisplay[50];

	wsprintf(polyTotalDisplay, _T("Total Polygons: %d"),polyCounts);
	wsprintf(polyVisibleDisplay, _T("Rendered Polygons: %d"), polysDrawn);
	wsprintf(namePrintDisplay, _T("Bradley Stone - CGP Year 2: 3D Software Renderer"));
	
	if(mouseControl == CAMERA)
		wsprintf(mouseControlDisplay, _T("Mouse Control: Camera"));
	else if(mouseControl == OBJECT)
		wsprintf(mouseControlDisplay, _T("Mouse Control: Objects"));
	else if(mouseControl == LIGHT)
		wsprintf(mouseControlDisplay, _T("Mouse Control: Light"));

	const int windowRight = renderWindow.right - 350;
	const int windowBottom = renderWindow.bottom - 30;

	TextOut(hdc, 10, 5, polyTotalDisplay, lstrlen(polyTotalDisplay));
	TextOut(hdc, 10, windowBottom - 25, mouseControlDisplay, lstrlen(mouseControlDisplay));
	TextOut(hdc, 10, windowBottom, namePrintDisplay, lstrlen(namePrintDisplay));
	TextOut(hdc, windowRight, 5, renderingMode, lstrlen(renderingMode));
	TextOut(hdc, 10, 30, polyVisibleDisplay, lstrlen(polyVisibleDisplay));
	TextOut(hdc, windowRight, 30, lightingMode, lstrlen(lightingMode));
	TextOut(hdc, renderWindow.right - 100, windowBottom, szFPS, lstrlen(szFPS));

	DeleteObject(SelectObject(hdc, hTmp));
}