#include "stdafx.h"
#include "Camera.h"

Camera::Camera(void)
{
	Copy(Vertex(0,0,0,1),0,0,0, RECT());
	UpdateView();
	UpdateScreen();
	UpdateFinal();
}

Camera::Camera(Vertex position, float xAngle, float yAngle, float zAngle, RECT screen)
{
	Copy(position,xAngle,yAngle,zAngle, screen);
	UpdateView();
	UpdateScreen();
	UpdateFinal();
}

Camera::Camera(const Camera &cam)
{
	CopyCam(cam._position, cam._xAngle,cam._yAngle, cam._zAngle, cam._screen, cam._screenM, cam._view, cam._final);
}

Camera::~Camera(void)
{

}

Vertex Camera::GetPosition(void)
{
	return _position;
}

void Camera::SetPosition(Vertex position)
{
	_position = position;
	UpdateView();
}

Matrix Camera::GetPipeline(void)
{
	return _final;
}

float Camera::GetXAngle(void)
{
	return _xAngle;
}

void Camera::SetXAngle(float xAngle)
{
	_xAngle = xAngle;
	UpdateView();
	UpdateFinal();
}

float Camera::GetYAngle(void)
{
	return _yAngle;
}

void Camera::SetYAngle(float yAngle)
{
	_yAngle = yAngle;
	UpdateView();
	UpdateFinal();
}

float Camera::GetZAngle(void)
{
	return _zAngle;
}

void Camera::SetZAngle(float zAngle)
{
	_zAngle = zAngle;
	UpdateView();
	UpdateFinal();
}

void Camera::SetXYZAngle(float xAngle, float yAngle, float zAngle)
{
	_xAngle = xAngle;
	_yAngle = yAngle;
	_zAngle = zAngle;
	UpdateView();
	UpdateFinal();
}

RECT Camera::GetScreen(void)
{
	return _screen;
}
		
void Camera::SetScreen(RECT screen)
{
	_screen = screen;
	UpdateScreen();
	UpdateFinal();
}

void Camera::Copy(Vertex position, float xAngle, float yAngle, float zAngle, RECT screen)
{
	_xAngle = xAngle;
	_yAngle = yAngle;
	_zAngle = zAngle;
	_position = position;
	_screen = screen;
	_perspective = Matrix::Perspective();
}

void Camera::CopyCam(Vertex position, float xAngle, float yAngle, float zAngle, RECT screen, Matrix screenM, Matrix view, Matrix finalM)
{
	_xAngle = xAngle;
	_yAngle = yAngle;
	_zAngle = zAngle;
	_position = position;
	_screen = screen;
	_perspective = Matrix::Perspective();
	_screenM = screenM;
	_view = view;
	_final = finalM;
}

void Camera::UpdateView(void)
{
	_view = Matrix::Inverse(Matrix::View(Vector3D(_position.x(),_position.y(),_position.z()), _xAngle, _yAngle, _zAngle));
}

void Camera::UpdateScreen(void)
{
	_screenM = Matrix::Screen(_screen.right, _screen.bottom);
}

void Camera::UpdateFinal(void)
{
	_final = _view * _perspective * _screenM;
}