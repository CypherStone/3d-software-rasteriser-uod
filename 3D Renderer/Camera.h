#pragma once
#include "stdafx.h"
#include "Matrix.h"

class Camera
{
	private:
		float _zAngle;
		float _yAngle;
		float _xAngle;
		float _depth;

		RECT _screen;

		Vertex _position;
		Matrix _screenM;
		Matrix _perspective;
		Matrix _view;
		Matrix _final;

		void Copy(Vertex position, float xAngle, float yAngle, float zAngle, RECT screen);
		void CopyCam(Vertex position, float xAngle, float yAngle, float zAngle, RECT screen, Matrix screenM, Matrix view, Matrix finalM);
		void UpdateView(void);
		void UpdateScreen(void);
		void UpdateFinal(void);

	public:
		Camera(void);
		Camera(Vertex position, float xAngle, float yAngle, float zAngle, RECT screen);
		Camera(const Camera &cam);
		~Camera(void);

		Vertex GetPosition(void);
		void SetPosition(Vertex position);
		Matrix GetPipeline(void);

		float GetXAngle(void);
		void SetXAngle(float xAngle);
		float GetYAngle(void);
		void SetYAngle(float yAngle);
		float GetZAngle(void);
		void SetZAngle(float zAngle);
		void SetXYZAngle(float xAngle, float yAngle, float zAngle);
		RECT GetScreen(void);
		void SetScreen(RECT screen);
};
