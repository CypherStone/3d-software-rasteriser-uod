#include "stdafx.h"


COLORREF Geometry::BarycentricInterpolation(Vertex main, Vertex a, Vertex b, Vertex c)
{
	// A lighterweight barycentric interpolation, less computations needed to get the same results.
	// Original source can be found here: http://www.gamedev.net/topic/621445-barycentric-coordinates-c-code-check/?view=findpost&p=4920278
	// I improved it further, by getting rid of the duplicate calculations.

	const float equationOne = b.y() - c.y();
	const float equationTwo = c.x() - b.x();
	const float equationThree = a.x() - c.x();
	const float equationFour = main.x() - c.x();
	const float equationFive = main.y() - c.y();

    const float multiplier = 1.0f / (equationOne * equationThree + equationTwo * (a.y() - c.y()));

    const float x = (equationOne * equationFour + equationTwo * equationFive) * multiplier;
    const float y = ((c.y() - a.y()) * equationFour + equationThree * equationFive) * multiplier;
    const float z = 1.0f - x - y;

	const int red = (int)((x * GetRValue(a.Colour())) + (y * GetRValue(b.Colour())) + (z * GetRValue(c.Colour())));
	const int green = (int)((x * GetGValue(a.Colour())) + (y * GetGValue(b.Colour())) + (z * GetGValue(c.Colour())));
	const int blue = (int)((x * GetBValue(a.Colour())) + (y * GetBValue(b.Colour())) + (z * GetBValue(c.Colour())));

	return Shading::ReturnColour(red, green, blue);
}

Vector3D Geometry::BarycentricVector(Vector3D main, Polygon3D poly)
{
	// A different implementation of Barycentric co-ordinates.
	// This one is more suited to vectors.
	Vector3D v0 = Vector3D::ToVector(poly.GetVertex(2)) - Vector3D::ToVector(poly.GetVertex(0));
	Vector3D v1 = Vector3D::ToVector(poly.GetVertex(1)) - Vector3D::ToVector(poly.GetVertex(0));
	Vector3D v2 = main - Vector3D::ToVector(poly.GetVertex(0));

	// Compute each vector dot product.
	float dotA = Vector3D::DotProduct(v0, v0);
	float dotB = Vector3D::DotProduct(v0, v1);
	float dotC = Vector3D::DotProduct(v0, v2);
	float dotD = Vector3D::DotProduct(v1, v1);
	float dotE = Vector3D::DotProduct(v1, v2);

	// Calculate the barycentric coordinates.
	float denominator = 1.0f / (dotA * dotD - dotB * dotB);
	float u = (dotD * dotC - dotB * dotE) * denominator;
	float v = (dotA * dotE - dotB * dotC) * denominator;

	// A simple check to see if the point is in triangle.
	 if((u >= 0) && (v >= 0) && (u + v <= 1))
		return Vector3D(u, v, 0);
	 else
		return Vector3D();
}

COLORREF Geometry::Interpolation(float main, float a, float b, COLORREF first, COLORREF second)
{
	// Linear colour interpolation.
	float t = abs(a - main) / abs(b - a);

	float step = 1.0f - t;

	int red =(int)(t * GetRValue(second) + step * GetRValue(first));
	int green =(int)(t * GetGValue(second) + step * GetGValue(first));
	int blue =(int)(t * GetBValue(second) + step * GetBValue(first));
	
	return Shading::ReturnColour(red, green, blue);
}