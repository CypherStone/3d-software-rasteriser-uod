#pragma once
#include "stdafx.h"

class Geometry
{
	public:
		static COLORREF BarycentricInterpolation(Vertex main, Vertex a, Vertex b, Vertex c);
		static Vector3D BarycentricVector(Vector3D main, Polygon3D poly);
		static COLORREF Interpolation(float main, float a, float b, COLORREF first, COLORREF second);
};