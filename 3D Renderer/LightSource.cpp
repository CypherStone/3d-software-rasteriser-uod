#include "LightSource.h"
#include "stdafx.h"

LightSource::LightSource(void)
{
	LightingComponents.ambientR = 0.0f;
	LightingComponents.ambientG = 0.0f;
	LightingComponents.ambientB = 0.0f;
	LightingComponents.diffuseR = 0.0f;
	LightingComponents.diffuseG = 0.0f;
	LightingComponents.diffuseB = 0.0f;
	LightingComponents.specularR = 0.0f;
	LightingComponents.specularG = 0.0f;
	LightingComponents.specularB = 0.0f;

	_specularExpo = 0.0f;
	_position = Vector3D(0,0,0);
}

LightSource::LightSource(const LightSource &light)
{
	Copy(light);
}

LightSource::LightSource(Material lighting, float specularExpo, Vector3D position)
{
	LightingComponents = lighting;
	_position = position;
	_specularExpo = specularExpo;
}

LightSource::~LightSource(void)
{
}
		
LightSource& LightSource::operator= (const LightSource &light)
{
	Copy(light);
	return *this;
}

Vector3D LightSource::Position(void)
{
	return _position;
}

void LightSource::Position(Vector3D v)
{
	_position = v;
}

float LightSource::SpecularExponent(void)
{
	return _specularExpo;
}

void LightSource::SpecularExponent(float expo)
{
	_specularExpo = expo;
}

void LightSource::Copy(const LightSource &light)
{
	LightingComponents = light.LightingComponents;
	_position = light._position;
	_specularExpo = light._specularExpo;
}