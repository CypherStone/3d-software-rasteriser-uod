#pragma once
#include "stdafx.h"

typedef struct 
{
	float ambientR;
	float ambientG;
	float ambientB;
	float diffuseR;
	float diffuseG;
	float diffuseB;
	float specularR;
	float specularG;
	float specularB;
}Material;


class LightSource
{
	private:
		float _specularExpo;
		Vector3D _position;
		void Copy(const LightSource &light);

	public:
		Material LightingComponents;
		LightSource(void);
		LightSource(const LightSource &light);
		LightSource(Material lighting, float specularExpo, Vector3D position);
		~LightSource(void);
		LightSource& operator= (const LightSource &light);

		Vector3D Position(void);
		void Position(Vector3D v);
		float SpecularExponent(void);
		void SpecularExponent(float expo);
};