#include "Matrix.h"
#include "stdafx.h"

Matrix::Matrix(void)
{
	IntializeMatrix();
}

Matrix::~Matrix()
{
}

Matrix::Matrix(const Matrix& m)
{
	Copy(m);
}

float Matrix::Get(int row, int col) const
{
	return pMatrix[row][col];
}

void Matrix::Set(int row, int col, float val)
{
	pMatrix[row][col] = val;
}

Matrix Matrix::Scalar(float xScale, float yScale, float zScale)
{
	Matrix scalar;

	scalar.Set(0, 0, xScale); 
	scalar.Set(1, 0, 0.0f);
	scalar.Set(2, 0, 0.0f);	
	scalar.Set(3, 0, 0.0f);
	scalar.Set(0, 1, 0.0f);	
	scalar.Set(1, 1, yScale);	
	scalar.Set(2, 1, 0.0f);	
	scalar.Set(3, 1, 0.0f);
	scalar.Set(0, 2, 0.0f);	
	scalar.Set(1, 2, 0.0f);	
	scalar.Set(2, 2, zScale);	
	scalar.Set(3, 2, 0.0f);
	scalar.Set(0, 3, 0.0f);	
	scalar.Set(1, 3, 0.0f);	
	scalar.Set(2, 3, 0.0f);	
	scalar.Set(3, 3, 1.0f);

	return scalar;
}  

Matrix Matrix::Translation(float xTranslation, float yTranslation, float zTranslation)
{
	Matrix translation;

	translation.Set(0, 0, 1.0f);	
	translation.Set(1, 0, 0.0f);	
	translation.Set(2, 0, 0.0f);	
	translation.Set(3, 0, xTranslation);
	translation.Set(0, 1, 0.0f);	
	translation.Set(1, 1, 1.0f);	
	translation.Set(2, 1, 0.0f);	
	translation.Set(3, 1, yTranslation);
	translation.Set(0, 2, 0.0f);	
	translation.Set(1, 2, 0.0f);	
	translation.Set(2, 2, 1.0f);	
	translation.Set(3, 2, zTranslation);
	translation.Set(0, 3, 0.0f);	
	translation.Set(1, 3, 0.0f);	
	translation.Set(2, 3, 0.0f);	
	translation.Set(3, 3, 1.0f);

	return translation;
}

Matrix Matrix::RotateX(float xRotate)
{
	Matrix xRotateMatrix;

	xRotateMatrix.Set(0, 0, 1.0f);	
	xRotateMatrix.Set(1, 0, 0.0f);			
	xRotateMatrix.Set(2, 0, 0.0f);			
	xRotateMatrix.Set(3, 0, 0.0f);
	xRotateMatrix.Set(0, 1, 0.0f);	
	xRotateMatrix.Set(1, 1, (cos(-xRotate)));	
	xRotateMatrix.Set(2, 1, (-sin(-xRotate)));	
	xRotateMatrix.Set(3, 1, 0.0f);
	xRotateMatrix.Set(0, 2, 0.0f);	
	xRotateMatrix.Set(1, 2, (sin(-xRotate)));	
	xRotateMatrix.Set(2, 2, (cos(-xRotate)));	
	xRotateMatrix.Set(3, 2, 0.0f);
	xRotateMatrix.Set(0, 3, 0.0f);	
	xRotateMatrix.Set(1, 3, 0.0f);			
	xRotateMatrix.Set(2, 3, 0.0f);			
	xRotateMatrix.Set(3, 3, 1.0f);

	return xRotateMatrix;
}

Matrix Matrix::RotateY(float yRotate)
{
	Matrix yRotateMatrix;

	yRotateMatrix.Set(0, 0, (cos(-yRotate)));	
	yRotateMatrix.Set(1, 0, 0.0f);	
	yRotateMatrix.Set(2, 0, (sin(-yRotate)));	
	yRotateMatrix.Set(3, 0, 0.0f);
	yRotateMatrix.Set(0, 1, 0.0f);			
	yRotateMatrix.Set(1, 1, 1.0f);	
	yRotateMatrix.Set(2, 1, 0);				
	yRotateMatrix.Set(3, 1, 0.0f);
	yRotateMatrix.Set(0, 2, (-sin(-yRotate)));	
	yRotateMatrix.Set(1, 2, 0.0f);	
	yRotateMatrix.Set(2, 2, (cos(-yRotate)));	
	yRotateMatrix.Set(3, 2, 0.0f);
	yRotateMatrix.Set(0, 3, 0.0f);			
	yRotateMatrix.Set(1, 3, 0.0f);	
	yRotateMatrix.Set(2, 3, 0.0f);			
	yRotateMatrix.Set(3, 3, 1.0f);

	return yRotateMatrix;
}

Matrix Matrix::RotateZ(float zRotate)
{
	Matrix zRotateMatrix;

	zRotateMatrix.Set(0, 0, (cos(-zRotate)));	
	zRotateMatrix.Set(1, 0, (-sin(-zRotate)));	
	zRotateMatrix.Set(2, 0, 0.0f);	
	zRotateMatrix.Set(3, 0, 0.0f);
	zRotateMatrix.Set(0, 1, (sin(-zRotate)));	
	zRotateMatrix.Set(1, 1, (cos(-zRotate)));	
	zRotateMatrix.Set(2, 1, 0.0f);	
	zRotateMatrix.Set(3, 1, 0.0f);
	zRotateMatrix.Set(0, 2, 0.0f);			
	zRotateMatrix.Set(1, 2, 0.0f);			
	zRotateMatrix.Set(2, 2, 1.0f);	
	zRotateMatrix.Set(3, 2, 0.0f);
	zRotateMatrix.Set(0, 3, 0.0f);			
	zRotateMatrix.Set(1, 3, 0.0f);			
	zRotateMatrix.Set(2, 3, 0.0f);	
	zRotateMatrix.Set(3, 3, 1.0f);

	return zRotateMatrix;
}

Matrix Matrix::RotateXYZ(float xRotate, float yRotate, float zRotate)
{
	return RotateX(xRotate) * RotateY(yRotate) * RotateZ(zRotate);
}

Matrix Matrix::Perspective(void)
{
	Matrix perspective;

	perspective.Set(0, 0, 1.0f);		
	perspective.Set(0, 1, 0.0f);		
	perspective.Set(0, 2, 0.0f);		
	perspective.Set(0, 3, 0.0f);
	perspective.Set(1, 0, 0.0f);		
	perspective.Set(1, 1, 1.0f);		
	perspective.Set(1, 2, 0.0f);		
	perspective.Set(1, 3, 0.0f);
	perspective.Set(2, 0, 0.0f);		
	perspective.Set(2, 1, 0.0f);		
	perspective.Set(2, 2, 1.0f);		
	perspective.Set(2, 3, 0.5f);
	perspective.Set(3, 0, 0.0f);		
	perspective.Set(3, 1, 0.0f);		
	perspective.Set(3, 2, 0.0f);		
	perspective.Set(3, 3, 0.0f);

	return perspective;
}

Matrix Matrix::Identity(void)
{
	Matrix identity;

	identity.Set(0, 0, 1.0f);		
	identity.Set(1, 0, 0.0f);		
	identity.Set(2, 0, 0.0f);		
	identity.Set(3, 0, 0.0f);
	identity.Set(0, 1, 0.0f);		
	identity.Set(1, 1, 1.0f);		
	identity.Set(2, 1, 0.0f);		
	identity.Set(3, 1, 0.0f);
	identity.Set(0, 2, 0.0f);		
	identity.Set(1, 2, 0.0f);		
	identity.Set(2, 2, 1.0f);		
	identity.Set(3, 2, 0.0f);
	identity.Set(0, 3, 0.0f);		
	identity.Set(1, 3, 0.0f);		
	identity.Set(2, 3, 0.0f);		
	identity.Set(3, 3, 1.0f);

	return identity;
}

Matrix Matrix::View(Vector3D position, float xAngle, float yAngle, float zAngle)
{
	return RotateXYZ(xAngle, yAngle, zAngle) * Translation(position.x(), position.y(), position.z());
}

Matrix Matrix::Screen(int width, int height)
{
	Matrix screenMatrix;

	float scaleY = (float) min(width, height) / 2;
	float scaleX = (- 1.0f * (float) min(width, height) / 2);

	screenMatrix.Set(0, 0, scaleY);				
	screenMatrix.Set(0, 1, 0.0f);				
	screenMatrix.Set(0, 2, 0.0f);						
	screenMatrix.Set(0, 3, 0.0f);
	screenMatrix.Set(1, 0, 0.0f);
	screenMatrix.Set(1, 1, scaleX);	
	screenMatrix.Set(1, 2, 0.0f);
	screenMatrix.Set(1, 3, 0.0f);	
	screenMatrix.Set(2, 0, 0.0f);	
	screenMatrix.Set(2, 1, 0.0f);
	screenMatrix.Set(2, 2, 1.0f);
	screenMatrix.Set(2, 3, 0.0f);	
	screenMatrix.Set(3, 0, (float)(width / 2));
	screenMatrix.Set(3, 1, (float)(height / 2));
	screenMatrix.Set(3, 2, 0.0f);
	screenMatrix.Set(3, 3, 1.0f);
	return screenMatrix;
}

Matrix Matrix::Inverse(Matrix &toInvert)
{
	Matrix inverse(Matrix::Identity());
	Matrix temp = toInvert;

	float t;
    int i, j, k, swap;
        
    for (i = 0; i < 4; i++) 
	{
        // look for largest element in column to be used as pivot.
        swap = i;
        for (j = i + 1; j < 4; j++)
		{
            if (fabs(temp.Get(j,i)) > fabs(temp.Get(i,i))) 
			{
                swap = j;
            }
        }
        
        if (swap != i) 
		{
            for (k = 0; k < 4; k++) 
			{
				t = temp.pMatrix[i][k];
                temp.pMatrix[i][k] = temp.pMatrix[swap][k];
                temp.pMatrix[swap][k] = t;
                
                t = inverse.Get(i, k);
				inverse.Set(i, k, inverse.Get(swap, k));
				inverse.Set(swap, k, t);
            }
        }
        
		if (temp.Get(i, i) == 0) 
		{
        // singular matrix no go
			return Matrix::Identity();
        }
        
		t = temp.Get(i, i);

        for (k = 0; k < 4; k++) 
		{
			temp.Set(i, k, temp.Get(i, k) / t);
			inverse.Set(i, k, inverse.Get(i, k) / t);
        }

        for (j = 0; j < 4; j++) 
		{
            if (j != i) 
			{
                t = temp.Get(j, i);
                for (k = 0; k < 4; k++) 
				{
					temp.Set(j, k, temp.Get(j, k) - temp.Get(i, k) * t);
					inverse.Set(j, k, inverse.Get(j, k) - inverse.Get(i, k) * t);
                }
            }
        }
    }

	return inverse;
}

Matrix Matrix::Transpose(Matrix &toTranspose)
{
	Matrix transposed;
 
    for(int i = 0; i < 4; i++)
    {
		for(int j = 0; j < 4; j++)
		{
			transposed.pMatrix[j][i] = toTranspose.pMatrix[i][j];
		}
    }
	return transposed;
}

Matrix& Matrix::operator= (const Matrix &other)
{
	Copy(other);
	return *this;
}

const Matrix Matrix::operator+= (const Matrix &other) const
{
	Matrix temp;

	temp.Set(0,0, (Get(0,0) + other.Get(0,0)));
	temp.Set(0,1, (Get(0,1) + other.Get(0,1)));
	temp.Set(0,2, (Get(0,2) + other.Get(0,2)));
	temp.Set(0,3, (Get(0,3) + other.Get(0,3)));
	temp.Set(1,0, (Get(1,0) + other.Get(1,0)));
	temp.Set(1,1, (Get(1,1) + other.Get(1,1)));
	temp.Set(1,2, (Get(1,2) + other.Get(1,2)));
	temp.Set(1,3, (Get(1,3) + other.Get(1,3)));
	temp.Set(2,0, (Get(2,0) + other.Get(2,0)));
	temp.Set(2,1, (Get(2,1) + other.Get(2,1)));
	temp.Set(2,2, (Get(2,2) + other.Get(2,2)));
	temp.Set(2,3, (Get(2,3) + other.Get(2,3)));
	temp.Set(3,0, (Get(3,0) + other.Get(3,0)));
	temp.Set(3,1, (Get(3,1) + other.Get(3,1)));
	temp.Set(3,2, (Get(3,2) + other.Get(3,2)));
	temp.Set(3,3, (Get(3,3) + other.Get(3,3)));

	return temp;
}

const Matrix Matrix::operator+ (const Matrix &other) const
{
	Matrix temp(*this);
	return (temp += other);
}

float &Matrix::operator()(int row, int col)
{
	return pMatrix[row][col];
}

const bool Matrix::operator== (const Matrix &other) const
{
	for(int i = 0; i < 4; i++)
	{
		for(int j = 0; j < 4; j++)
		{
			if(pMatrix[i][j] != other.pMatrix[i][j])
			{
				return false;
			}
		}
	}

	return true;
}

const bool Matrix::operator!= (const Matrix &other) const
{
	return !(this == &other);
}

const Matrix Matrix::operator*(const Matrix &other) const
{
	Matrix result;
	for (int i = 0; i < 4; i++)
	{
		for(int j = 0; j < 4; j++)
		{
			float value = 0.0f;
			for (int k = 0; k < 4; k++)
			{
				value += pMatrix[i][k] * other.pMatrix[k][j];
			}
			result.pMatrix[i][j] = value;
		}
	}
	
	return result;
}

const Matrix operator*(float val, const Matrix &other)
{
	Matrix result(other);
	for(int row = 0; row < 4; row++)
	{
		for(int col = 0; col < 4; col++)
		{
			result.Set(row, col,val * other.Get(row, col));
		}
	}

	return result;
}

const Matrix operator*(const Matrix &other, float val)
{
	return val * other;
}

const Vector3D Matrix::operator*(const Vertex &p) const
{
	Vector3D result;
	float x = 0.0f;
	float y = 0.0f;
	float z = 0.0f;
	float w = 0.0f;
	x = (pMatrix[0][0] * p.x()) + (pMatrix[1][0] * p.y()) + (pMatrix[2][0] * p.z()) + (pMatrix[3][0] * p.w());
	y = (pMatrix[0][1] * p.x()) + (pMatrix[1][1] * p.y()) + (pMatrix[2][1] * p.z()) + (pMatrix[3][1] * p.w());
	z = (pMatrix[0][2] * p.x()) + (pMatrix[1][2] * p.y()) + (pMatrix[2][2] * p.z()) + (pMatrix[3][2] * p.w());
	w = (pMatrix[0][3] * p.x()) + (pMatrix[1][3] * p.y()) + (pMatrix[2][3] * p.z()) + (pMatrix[3][3] * p.w());
	result = Vector3D(x, y, z, w);
	return result;
}

const Vector3D Matrix::operator*(const Vector3D &p) const
{
	Vector3D result;
	float x = 0.0f;
	float y = 0.0f;
	float z = 0.0f;
	float w = 0.0f;
	x = (pMatrix[0][0] * p.x()) + (pMatrix[1][0] * p.y()) + (pMatrix[2][0] * p.z()) + (pMatrix[3][0] * p.w());
	y = (pMatrix[0][1] * p.x()) + (pMatrix[1][1] * p.y()) + (pMatrix[2][1] * p.z()) + (pMatrix[3][1] * p.w());
	z = (pMatrix[0][2] * p.x()) + (pMatrix[1][2] * p.y()) + (pMatrix[2][2] * p.z()) + (pMatrix[3][2] * p.w());
	w = (pMatrix[0][3] * p.x()) + (pMatrix[1][3] * p.y()) + (pMatrix[2][3] * p.z()) + (pMatrix[3][3] * p.w());
	result = Vector3D(x, y, z, w);
	return result;
}

void Matrix::IntializeMatrix(void)
{
	pMatrix[0][0] = 0.0f;
	pMatrix[0][1] = 0.0f;
	pMatrix[0][2] = 0.0f;
	pMatrix[0][3] = 0.0f;
	pMatrix[1][0] = 0.0f;
	pMatrix[1][1] = 0.0f;
	pMatrix[1][2] = 0.0f;
	pMatrix[1][3] = 0.0f;
	pMatrix[2][0] = 0.0f;
	pMatrix[2][1] = 0.0f;
	pMatrix[2][2] = 0.0f;
	pMatrix[2][3] = 0.0f;
	pMatrix[3][0] = 0.0f;
	pMatrix[3][1] = 0.0f;
	pMatrix[3][2] = 0.0f;
	pMatrix[3][3] = 0.0f;
}

void Matrix::Copy(const Matrix& m)
{
	pMatrix[0][0] = m.pMatrix[0][0];
	pMatrix[0][1] = m.pMatrix[0][1];
	pMatrix[0][2] = m.pMatrix[0][2];
	pMatrix[0][3] = m.pMatrix[0][3];
	pMatrix[1][0] = m.pMatrix[1][0];
	pMatrix[1][1] = m.pMatrix[1][1];
	pMatrix[1][2] = m.pMatrix[1][2];
	pMatrix[1][3] = m.pMatrix[1][3];
	pMatrix[2][0] = m.pMatrix[2][0];
	pMatrix[2][1] = m.pMatrix[2][1];
	pMatrix[2][2] = m.pMatrix[2][2];
	pMatrix[2][3] = m.pMatrix[2][3];
	pMatrix[3][0] = m.pMatrix[3][0];
	pMatrix[3][1] = m.pMatrix[3][1];
	pMatrix[3][2] = m.pMatrix[3][2];
	pMatrix[3][3] = m.pMatrix[3][3];
}