#pragma once 
#include "stdafx.h"
#include "Vector3D.h"

class Matrix
{
   public:
	   //Creates a basic 2 Dimensional matrix.
	   Matrix(void);
	   //Creates a custom sized matrix.
	   Matrix(int,int);
	   Matrix(float, float, float, float);
	   Matrix(const Matrix&);
	   ~Matrix();

	   float Get(int, int) const;
	   void Set(int, int, float);
	   float &operator()(int, int);
	   Matrix& operator= (const Matrix &other);
	   const Matrix operator+= (const Matrix &other) const;
	   const Matrix operator+ (const Matrix &other) const;
	   const bool operator== (const Matrix &other) const;
	   const bool operator!= (const Matrix &other) const;
	   const Matrix operator*(const Matrix &other) const;
	   const friend Matrix operator*(const float, const Matrix &other);
	   const friend Matrix operator*(const Matrix &other, const float);
	   const Vector3D operator*(const Vertex &p) const;
	   const Vector3D operator*(const Vector3D &p) const;

	   static Matrix Inverse(Matrix &toInvert);
	   static Matrix Transpose(Matrix &toTranspose);
	   static Matrix Scalar(float xScale, float yScale, float zScale);
	   static Matrix Translation(float xTranslation, float yTranslation, float zTranslation);
	   static Matrix RotateX(float xRotate);
	   static Matrix RotateY(float yRotate);
	   static Matrix RotateZ(float zRotate);
	   static Matrix RotateXYZ(float xRotate, float yRotate, float zRotate);
	   static Matrix Perspective(void);
	   static Matrix Identity(void);
	   static Matrix View(Vector3D position, float xAngle, float yAngle, float zAngle);
	   static Matrix Screen(int width, int height);
	
	private:
		float pMatrix[4][4];
		void AllocateMemory(void);
		void IntializeMatrix(void);
		void Copy(const Matrix&);
};