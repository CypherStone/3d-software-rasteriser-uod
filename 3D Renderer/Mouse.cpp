#include "Mouse.h"
#include "stdafx.h"

Mouse::Mouse(void)
{
	InitializeMouse(0,0);
}

Mouse::Mouse(float a, float b)
{
	InitializeMouse(a,b);
}

Mouse::~Mouse(void)
{

}

bool Mouse::GetLClick(void) const
{
	return lClick;
}

bool Mouse::GetRClick(void) const
{
	return rClick;
}

void Mouse::SetLClick(bool click)
{
	lClick = click;
}

void Mouse::SetRClick(bool click)
{
	rClick = click;
}

float Mouse::GetX(void) const
{
	return x;
}

float Mouse::GetY(void) const
{
	return y;
}

void Mouse::SetX(float a)
{
	x = a;
}

void Mouse::SetY(float a)
{
	y = a;
}

float Mouse::GetMemX(void) const
{
	return mX;
}

float Mouse::GetMemY(void) const
{
	return mY;
}

void Mouse::SetMemX(float a)
{
	mX = a;
}

void Mouse::SetMemY(float a)
{
	mY = a;
}

Mouse& Mouse::operator= (const Mouse &other)
{
	Copy(other);
	return *this;
}

void Mouse::InitializeMouse(float a, float b)
{
	x = a;
	y = b;
	mX = 0;
	mY = 0;
	lClick = false;
	rClick = false;
}

void Mouse::Copy(const Mouse& other)
{
	x = other.GetX();
	y = other.GetY();
	mX = other.GetMemX();
	mY = other.GetMemY();
	lClick = other.GetLClick();
	rClick = other.GetRClick();
}