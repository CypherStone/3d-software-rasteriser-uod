#pragma once
#include "stdafx.h"

class Mouse
{
	public:
		Mouse(void);
		Mouse(float,float);
		Mouse(const Mouse&);
		~Mouse();
	
		bool GetLClick(void) const;
		bool GetRClick(void) const;
		void SetLClick(bool);
		void SetRClick(bool);

		float GetX(void) const;
		float GetY(void) const;
		void SetX(float);
		void SetY(float);
		
		float GetMemX(void) const;
		float GetMemY(void) const;
		void SetMemX(float);
		void SetMemY(float);

		Mouse& operator= (const Mouse &other);

	private:
		float x,y;
		float mX,mY;
		bool lClick, rClick;

		void InitializeMouse(float,float);
		void Copy(const Mouse&);
};