#include "Object3D.h"
#include "stdafx.h"

Object3D::Object3D(void)
{
	Init();
}

Object3D::Object3D(std::vector<Polygon3D> poly)
{
	polys = poly;
	_polycount = poly.size();
}

Object3D::Object3D(const Object3D& v)
{
	Copy(v);
}

Object3D::~Object3D(void)
{
}

Polygon3D& Object3D::GetPolygon(int i)
{
	return polys.at(i);
}

void Object3D::AddPolygon(const Polygon3D poly)
{
	polys.push_back(poly);
	_polycount++;
}

int Object3D::PolygonCount(void)
{
	return _polycount;
}

void Object3D::ObjectToMatrix(Matrix matrix)
{
	for(int poly = 0; poly < (int)polys.size(); poly++)
	{
		polys[poly].SetVertex(0, matrix * polys[poly].GetVertex(0));
		polys[poly].SetVertex(1, matrix * polys[poly].GetVertex(1));
		polys[poly].SetVertex(2, matrix * polys[poly].GetVertex(2));
	}
}

void Object3D::ObjectModelToWorld(Matrix world, Matrix inverseTranspose)
{
	for(int poly = 0; poly < (int)polys.size(); poly++)
	{
		polys[poly].SetVertex(0, world * polys[poly].GetVertex(0));
		polys[poly].SetVertex(1, world * polys[poly].GetVertex(1));
		polys[poly].SetVertex(2, world * polys[poly].GetVertex(2));
		polys[poly].GetNormalVertex(0) = inverseTranspose * polys[poly].GetNormalVertex(0);
		polys[poly].GetNormalVertex(1) = inverseTranspose * polys[poly].GetNormalVertex(1);
		polys[poly].GetNormalVertex(2) = inverseTranspose * polys[poly].GetNormalVertex(2);
		polys[poly].Normal(inverseTranspose * polys[poly].Normal());
	}
}

void Object3D::SetObjectNormals(void)
{
	for(int poly = 0; poly < (int)polys.size(); poly++)
	{
		Vector3D vertexNormal1, vertexNormal2, vertexNormal3;
		int normalDividerA = 0;
		int normalDividerB = 0;
		int normalDividerC = 0;

		for(int iPoly = 0; iPoly < (int)polys.size(); iPoly++)
		{
			SurroundingPolygons(polys[iPoly], Vector3D::ToVector(polys[poly].GetVertex(0)), normalDividerA, vertexNormal1);
			SurroundingPolygons(polys[iPoly], Vector3D::ToVector(polys[poly].GetVertex(1)), normalDividerB, vertexNormal2);
			SurroundingPolygons(polys[iPoly], Vector3D::ToVector(polys[poly].GetVertex(2)), normalDividerC, vertexNormal3);			
		}

		polys[poly].SetNormalVertex(0, (vertexNormal1 / normalDividerA).Normalize());
		polys[poly].SetNormalVertex(1, (vertexNormal2 / normalDividerB).Normalize());
		polys[poly].SetNormalVertex(2, (vertexNormal3 / normalDividerC).Normalize());
	}
}

void Object3D::CullingToLightingToDepth(Camera camera, LightSource light, RenderModes renderModes, LightingModes shade)
{
	std::vector<Polygon3D> tempPolys;

	for(int poly = 0; poly < (int)polys.size(); poly++)
	{
		Vector3D eyeToPolygon = Vector3D::ToVector(polys[poly].GetVertex(0)) - Vector3D::ToVector(camera.GetPosition());
		float dotProduct = Vector3D::DotProduct(polys[poly].Normal(), eyeToPolygon.Normalize());
		
		if (dotProduct > 0)
		{	
			Polygon3D tempPoly = polys[poly];
			tempPoly.AverageZ(tempPoly.GetVertex(0).z() + tempPoly.GetVertex(1).z() + tempPoly.GetVertex(2).z() / 3);

			if(tempPoly.AverageZ() > camera.GetPosition().z())
			{
				if(renderModes == PERPOLYSHADE)
					tempPoly.PolygonColour(Shading::CalculateBlinnPhongLight(camera, light, ObjMaterial, tempPoly.Normal(), shade));
				else if(renderModes == PERVERTEXSHADE)
				{
					tempPoly.GetVertex(0).Colour(Shading::CalculateBlinnPhongLight(camera, light, ObjMaterial, tempPoly.GetNormalVertex(0), shade));
					tempPoly.GetVertex(1).Colour(Shading::CalculateBlinnPhongLight(camera, light, ObjMaterial, tempPoly.GetNormalVertex(1), shade));
					tempPoly.GetVertex(2).Colour(Shading::CalculateBlinnPhongLight(camera, light, ObjMaterial, tempPoly.GetNormalVertex(2), shade));
				}
				
				tempPolys.push_back(tempPoly);
				
			}
		}
	}
	std::sort(tempPolys.begin(), tempPolys.end());
	polys = tempPolys;
	_polycount = polys.size();
}

void Object3D::SurroundingPolygons(Polygon3D polygon, Vector3D compare, int &polyCount, Vector3D &normalVertex)
{	
	if(Vector3D::ToVector(polygon.GetVertex(0)) == compare)
	{
		polyCount++;
		normalVertex = normalVertex + polygon.Normal();
	}
	else if(Vector3D::ToVector(polygon.GetVertex(1)) == compare)
	{
		polyCount++;
		normalVertex = normalVertex + polygon.Normal();
	}
	else if(Vector3D::ToVector(polygon.GetVertex(2)) == compare)
	{
		polyCount++;
		normalVertex = normalVertex + polygon.Normal();
	}
}

Object3D& Object3D::operator=(const Object3D &rhs)
{
	if(this != &rhs)
	{
		Copy(rhs);
	}
	return *this;
}

void Object3D::SetTexture(HBITMAP texture)
{
	_texture = texture;
}

HBITMAP Object3D::GetTexture(void)
{
	return _texture;
}

void Object3D::SetTextured(bool texture)
{
	_gotTexture = texture;
}

bool Object3D::IsTextured(void)
{
	return _gotTexture;
}

void Object3D::XAngle(float angle)
{
	_xAngle = angle;
}

void Object3D::YAngle(float angle)
{
	_yAngle = angle;
}

void Object3D::ZAngle(float angle)
{
	_zAngle = angle;
}

float Object3D::XAngle(void)
{
	return _xAngle;
}

float Object3D::YAngle(void)
{
	return _yAngle;
}

float Object3D::ZAngle(void)
{
	return _zAngle;
}

Vector3D Object3D::Position(void)
{
	return _position;
}

void Object3D::Position(Vector3D pos)
{
	_position = pos;
}

void Object3D::Copy(const Object3D& v)
{
	polys = v.polys;
	_polycount = v._polycount;
	_xAngle = v._xAngle;
	_yAngle = v._yAngle;
	_zAngle = v._zAngle;
	_position = v._position;
	ObjMaterial = v.ObjMaterial;
	_texture = v._texture;
	_gotTexture = v._gotTexture;
}

void Object3D::Init(void)
{
	_polycount = 0;
	_xAngle = 0;
	_yAngle = 0;
	_zAngle = 0;
	_gotTexture = false;
	_position = Vector3D(0,0,0);
	ObjMaterial.ambientR = 0.0f;
	ObjMaterial.ambientG = 0.0f;
	ObjMaterial.ambientB = 0.0f;
	ObjMaterial.diffuseR = 0.0f;
	ObjMaterial.diffuseG = 0.0f;
	ObjMaterial.diffuseB = 0.0f;
	ObjMaterial.specularR = 0.0f;
	ObjMaterial.specularG = 0.0f;
	ObjMaterial.specularB = 0.0f;
}
