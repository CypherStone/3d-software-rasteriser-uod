#pragma once
#include "stdafx.h"
#include "RenderEngine.h"

class Object3D
{
	public:
		Object3D(void);
		Object3D(std::vector<Polygon3D> poly);
		Object3D(const Object3D& v);
		~Object3D(void);

		int PolygonCount(void);
		void SetObjectNormals(void);

		Polygon3D& GetPolygon(int);
		void AddPolygon(const Polygon3D);
		void SetPolygonVertex(int polyNo, int vertexNo, Vector3D value);
		void ObjectToMatrix(Matrix matrix);
		void ObjectModelToWorld(Matrix world, Matrix inverseTranspose);
		Object3D& operator= (const Object3D &rhs);
		void CullingToLightingToDepth(Camera camera, LightSource light, RenderModes renderModes, LightingModes shade);
		void SurroundingPolygons(Polygon3D polygon, Vector3D compare, int &polyCount, Vector3D &normalVertex);

		void SetTexture(HBITMAP texture);
		HBITMAP GetTexture(void);
		void SetTextured(bool texture);
		bool IsTextured(void);
		void XAngle(float angle);
		float XAngle(void);
		void YAngle(float angle);
		float YAngle(void);
		void ZAngle(float angle);
		float ZAngle(void);

		Vector3D Position(void);
		void Position(Vector3D pos);
	
		Material ObjMaterial;
	private:
		Vector3D _position;
		float _xAngle;
		float _yAngle;
		float _zAngle;
		HBITMAP _texture;
		bool _gotTexture;
		std::vector<Polygon3D> polys;
		int _polycount;
		void Init();
		void Copy(const Object3D& v);

};