#include "stdafx.h"
#include "Polygon.h"

Polygon3D::Polygon3D(void)
{
	_vertex[0] = Vertex(0.0f,0.0f,0.0f,1);
	_vertex[1] = Vertex(0.0f,0.0f,0.0f,1);
	_vertex[2] = Vertex(0.0f,0.0f,0.0f,1);
	_vertexN[0] = Vector3D(0.0f,0.0f,0.0f,1);
	_vertexN[1] = Vector3D(0.0f,0.0f,0.0f,1);
	_vertexN[2] = Vector3D(0.0f,0.0f,0.0f,1);
	_texel[0] = Vector3D(0.0f,0.0f,0.0f,1);
	_texel[1] = Vector3D(0.0f,0.0f,0.0f,1);
	_texel[2] = Vector3D(0.0f,0.0f,0.0f,1);
	_polyColour = RGB(0, 0, 0);
	_averageZ = 0;
}

Polygon3D::Polygon3D(std::vector<Vertex> &v)
{
	_vertex[0] = v.at(0);
	_vertex[1] = v.at(1);
	_vertex[2] = v.at(2);
	_vertexN[0] = Vector3D(0.0f,0.0f,0.0f,1);
	_vertexN[1] = Vector3D(0.0f,0.0f,0.0f,1);
	_vertexN[2] = Vector3D(0.0f,0.0f,0.0f,1);
	_texel[0] = Vector3D(0.0f,0.0f,0.0f,1);
	_texel[1] = Vector3D(0.0f,0.0f,0.0f,1);
	_texel[2] = Vector3D(0.0f,0.0f,0.0f,1);
	_polyColour = RGB(0, 0, 0);
	_averageZ = 0;
}

Polygon3D::Polygon3D(const Polygon3D& v)
{
	Copy(v);
}

Polygon3D::~Polygon3D(void)
{
}

COLORREF Polygon3D::PolygonColour(void)
{
	return _polyColour;
}

void Polygon3D::PolygonColour(COLORREF colour)
{
	_polyColour = colour;
}

void Polygon3D::Dehemogenize(void)
{
	_vertex[0].x(_vertex[0].x() / _vertex[0].w());
	_vertex[0].y(_vertex[0].y() / _vertex[0].w());
	_vertex[0].z(_vertex[0].z() / _vertex[0].w());
	_vertex[0].w(_vertex[0].w() / _vertex[0].w());
	_vertex[1].x(_vertex[1].x() / _vertex[1].w());
	_vertex[1].y(_vertex[1].y() / _vertex[1].w());
	_vertex[1].z(_vertex[1].z() / _vertex[1].w());
	_vertex[1].w(_vertex[1].w() / _vertex[1].w());
	_vertex[2].x(_vertex[2].x() / _vertex[2].w());
	_vertex[2].y(_vertex[2].y() / _vertex[2].w());
	_vertex[2].z(_vertex[2].z() / _vertex[2].w());
	_vertex[2].w(_vertex[2].w() / _vertex[2].w());
}

Vector3D Polygon3D::Normal(void)
{
	return _normal;
}

void Polygon3D::Normal(Vector3D value)
{
	_normal = value;
}

float Polygon3D::AverageZ(void)
{
	return _averageZ;
}

void Polygon3D::AverageZ(float z)
{
	_averageZ = z;
}

Vertex &Polygon3D::GetVertex(int vertexNo)
{
	return _vertex[vertexNo];
}

void Polygon3D::SetVertex(int vertexNo, Vector3D value)
{
	_vertex[vertexNo] = value;
}

Vector3D &Polygon3D::GetNormalVertex(int vertexNo)
{
	return _vertexN[vertexNo];
}

void Polygon3D::SetNormalVertex(int vertexNo, Vector3D value)
{
	_vertexN[vertexNo] = value;
}

Vector3D &Polygon3D::GetTexel(int i)
{
	return _texel[i];
}

void Polygon3D::SetTexel(int vertexNo, Vector3D value)
{
	_texel[vertexNo] = value;
}

Polygon3D& Polygon3D::operator=(const Polygon3D &rhs)
{
	Copy(rhs);
	return *this;
}

void Polygon3D::Copy(const Polygon3D& v)
{
	_vertex[0] = v._vertex[0];
	_vertex[1] = v._vertex[1];
	_vertex[2] = v._vertex[2];
	_vertexN[0] = v._vertexN[0];
	_vertexN[1] = v._vertexN[1];
	_vertexN[2] = v._vertexN[2];
	_texel[0] = v._texel[0];
	_texel[1] = v._texel[1];
	_texel[2] = v._texel[2];
	_normal = v._normal;
	_polyColour = v._polyColour;
	_averageZ = v._averageZ;
}

const bool Polygon3D::operator<(const Polygon3D &other) const
{		
	if (_averageZ > other._averageZ)
		return true;

	return false;
}
