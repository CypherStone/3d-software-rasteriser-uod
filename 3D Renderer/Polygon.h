#pragma once
#include "stdafx.h"
#include "Vertex.h"

class Polygon3D
{
	public:
		Polygon3D(void);
		Polygon3D(std::vector<Vertex> &v);
		Polygon3D(const Polygon3D& v);
		~Polygon3D(void);
		
		void Dehemogenize(void);

		COLORREF PolygonColour(void);
		void PolygonColour(COLORREF colour);
		Vector3D Normal(void);
		void Normal (Vector3D);
		Vertex &GetVertex(int);
		void SetVertex(int vertexNo, Vector3D value);
		Vector3D &GetNormalVertex(int);
		void SetNormalVertex(int vertexNo, Vector3D value);
		Vector3D &GetTexel(int);
		void SetTexel(int vertexNo, Vector3D value);
		float AverageZ(void);
		void AverageZ(float z);

		Polygon3D& operator= (const Polygon3D &rhs);
		const bool operator<(const Polygon3D &other) const;

	private:
		float _averageZ;
		COLORREF _polyColour;
		Vertex _vertex[3];
		Vector3D _vertexN[3];
		Vector3D _texel[30];
		Vector3D _normal;
		void Init();
		void Copy(const Polygon3D& v);

};