﻿#include "RenderEngine.h"
#include "stdafx.h"

RenderEngine::RenderEngine(void)
{
	// Do Nothing.
}

RenderEngine::RenderEngine(HDC &hdc, std::vector<Object3D> &objects, Camera &camera, LightSource &light, RenderModes &mode, LightingModes &shade)
{
	_hdc = hdc;
	_objects = objects;
	_camera = camera;
	_light = light;
	_modes = mode;
	_shade = shade;

	// After storing the variables, it is time to render the objects.
	RasterizerPipeline();
}

RenderEngine::~RenderEngine(void)
{
	// Do Nothing.
}

void RenderEngine::RasterizerPipeline(void)
{
	_totalPolys = 0;
	_renderedPolys = 0;

	for(int obj = 0; obj < (int)_objects.size(); obj++)
	{
		_totalPolys += _objects[obj].PolygonCount();
		
		// This is the model to world calculations.
		Matrix modelWorldMatrix = Matrix::RotateXYZ(_objects[obj].XAngle(), _objects[obj].YAngle(), _objects[obj].ZAngle()) * Matrix::Translation(_objects[obj].Position().x(), _objects[obj].Position().y(), _objects[obj].Position().z());

		// This computes the modelWorldMatrix to both regular vertexes and to the normals.
		_objects[obj].ObjectModelToWorld(modelWorldMatrix, Matrix::Inverse(Matrix::Transpose(modelWorldMatrix)));		
		
		// Handles the polygon culling, lighting and depth sort of the object.
		_objects[obj].CullingToLightingToDepth(_camera, _light, _modes, _shade);

		// This gets the final matrix for the graphics pipeline - it is a combination of view, perspective and screen.
		_objects[obj].ObjectToMatrix(_camera.GetPipeline());

		// The object is now ready to be rendered.
		RenderObject(_hdc, _objects[obj], _camera, _light, _modes, _shade); 
	}
}

void RenderEngine::IncrementEvaluate(const int a, const int b, int &incrementA, int &incrementB)
{
	if (a > b) 
	{ 
		incrementA = -1; 
		incrementB = -1; 
	}
	else
	{
		incrementA = 1; 
		incrementB = 1; 
	}
}

void RenderEngine::DrawLine(HDC &hdc, Vertex a, Vertex b, class Scanline &scanline, RenderModes mode)
{	 
	// Based on the Bresenham's theory of line drawing with minor optimisation.
	int deltaX = abs((int)a.x() - (int)b.x());
	int deltaY = abs((int)a.y() - (int)b.y());

	int xIncrementA, xIncrementB, yIncrementA, yIncrementB, denominator, numerator, numeratorAdd, pixelCount;
	int x = (int)a.x();
	int y = (int)a.y();

	IncrementEvaluate(x, (int)b.x(), xIncrementA, xIncrementB);
	IncrementEvaluate(y, (int)b.y(), yIncrementA, yIncrementB);

	if (deltaX > deltaY)  
	{  
		xIncrementA = 0;   
		yIncrementB = 0;                     
		denominator = deltaX;
		numerator = deltaX / 2;
		numeratorAdd = deltaY;
		pixelCount = deltaX;        
	}
	else                             
	{
		xIncrementB = 0;                        
		yIncrementA = 0;    
		denominator = deltaY;
		numerator = deltaY / 2;
		numeratorAdd = deltaX;
		pixelCount = deltaY;    
	}

	for (int currentPixel = 0; currentPixel <= pixelCount; currentPixel++)
	{	
		if(mode == WIREFRAME)
			// Draws the pixel if it is on WireFrame mode.
			SetPixelV(hdc, x, y, RGB(255,255,255));
		else
			// Adds the point to the scanline edge table.
			scanline.AddScanline(scanline.GetScanlineMaxY() - y, x);

		numerator += numeratorAdd;   
		if (numerator >= denominator)           
		{
			numerator -= denominator;            
			x += xIncrementA;                 
			y += yIncrementA;            
		}

		x += xIncrementB;                       
		y += yIncrementB;                      
	}
}

void RenderEngine::RenderObject(HDC &hdc, Object3D obj, Camera cam, LightSource light, RenderModes mode, LightingModes shade)
{
	// Stores a temporary render mode incase the object is not textured.
	// If it isn't textured, it will display a wireframe version.
	RenderModes newRenderMode = mode;
	if(newRenderMode == TEXTURED && !obj.IsTextured())
		newRenderMode = WIREFRAME;

	for(int polygons = 0; polygons < obj.PolygonCount(); polygons++)
	{	
		obj.GetPolygon(polygons).Dehemogenize();
		// Doesn't render the polygon if it is out of the screen.
		if(ObjectClipping(obj.GetPolygon(polygons), cam.GetScreen()))
		{
			_renderedPolys++;
			Scanline scanline(obj.GetPolygon(polygons));
		
			// Draws the polygon outlines, or adds it to the scanline edge table.
			DrawLine(hdc, obj.GetPolygon(polygons).GetVertex(0), obj.GetPolygon(polygons).GetVertex(1), scanline, newRenderMode);
			DrawLine(hdc, obj.GetPolygon(polygons).GetVertex(1), obj.GetPolygon(polygons).GetVertex(2), scanline, newRenderMode);
			DrawLine(hdc, obj.GetPolygon(polygons).GetVertex(2), obj.GetPolygon(polygons).GetVertex(0), scanline, newRenderMode);
			
			// Decides the shading mode.
			if(newRenderMode == PERPOLYSHADE)
				Shading::ScanlinePolyFill(hdc, scanline, obj.GetPolygon(polygons));
			else if(newRenderMode == PERVERTEXSHADE)
				Shading::ScanlineVertexFill(hdc, scanline, obj.GetPolygon(polygons));
			else if(newRenderMode == TEXTURED)
				Shading::ScanlineTextureFill(hdc, scanline, obj.GetPolygon(polygons), obj.GetTexture());
		}
	}
}

bool RenderEngine::ObjectClipping(Polygon3D poly, RECT screen)
{
	bool visible = false;
	if(poly.GetVertex(0).x() >= screen.left && poly.GetVertex(0).y() >= screen.top && poly.GetVertex(0).x() <= screen.right && poly.GetVertex(0).y() <= screen.bottom)
	{
		visible = true;
	}
	else if(poly.GetVertex(1).x() >= screen.left && poly.GetVertex(1).y() >= screen.top && poly.GetVertex(1).x() <= screen.right && poly.GetVertex(1).y() <= screen.bottom && !visible)
	{
		visible = true;
	}
	else if(poly.GetVertex(2).x() >= screen.left && poly.GetVertex(2).y() >= screen.top && poly.GetVertex(2).x() <= screen.right && poly.GetVertex(2).y() <= screen.bottom && !visible)
	{
		visible = true;
	}
	return visible;
}

int RenderEngine::GetTotalPolygons(void)
{
	return _totalPolys;
}
		
int RenderEngine::GetRenderedPolygons(void)
{
	return _renderedPolys;
}