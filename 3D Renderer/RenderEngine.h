#pragma once
#include "stdafx.h"
#include "Object3D.h"
#include "LightSource.h"

enum RenderModes
{
	WIREFRAME,
	PERPOLYSHADE,
	PERVERTEXSHADE,
	TEXTURED
};

enum LightingModes
{
	AMBIENT,
	DIFFUSE,
	SPECULAR
};

class RenderEngine
{
	private:
		static void IncrementEvaluate(const int a, const int b, int &incrementA, int &incrementB);
		static COLORREF CalculateBlinnPhongLight(Camera camera, class LightSource light, Material mat, Vector3D vector, LightingModes shade);
		static void DrawLine(HDC &hdc, Vertex a, Vertex b, Scanline &scanline, RenderModes mode);
		void RenderObject(HDC &hdc, class Object3D obj, Camera cam, class LightSource light, RenderModes mode, LightingModes shade);
		static bool ObjectClipping(Polygon3D poly, RECT screen);

		void RasterizerPipeline(void);

		HDC _hdc;
		std::vector<class Object3D> _objects;
		RenderModes _modes;
		LightingModes _shade;
		Camera _camera;
		class LightSource _light;
		int _totalPolys;
		int _renderedPolys;

	public:
		RenderEngine(void);
		RenderEngine(HDC &hdc, std::vector<Object3D> &objects, Camera &camera, LightSource &light, RenderModes &mode, LightingModes &shade);
		~RenderEngine(void);
		int GetTotalPolygons(void);
		int GetRenderedPolygons(void);
};
