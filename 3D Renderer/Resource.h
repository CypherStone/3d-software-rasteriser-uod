//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by Resource.rc
//
#define IDR_MAIN_MENU                   101
#define IDD_ABOUTBOX                    103
#define IDM_FILE_ABOUT					104
#define IDM_FILE_NEW                    40001
#define ID_HELP_ABOUT                   40002
#define IDM_FILE_EXIT                   40006

#define IDC_TOOLBOX                     31000
#define IDC_MOUSECONTROL				31001
#define IDC_RENDERSET					31002
#define IDC_RENDERMODESB				31003
#define IDC_LIGHTMODESB					31004
#define IDC_DEMOMODECB					31005
#define IDT_FRAMES						31006

#define IDC_LIGHTAMBTEXT		        30001
#define IDC_LIGHTCOMPONENTSB			30007
#define IDC_LIGHTDIFTEXT				30002
#define IDC_LIGHTSPECTEXT				30003
#define IDC_LIGHTSETMAT					30005
#define IDC_LIGHTEXPOTEXT				30006

#define IDC_LOADOBJECT                  60001
#define IDC_COLOUROBJECT                60002
#define IDC_OBJAMBTEXT		            60003
#define IDC_OBJDIFTEXT					60004
#define IDC_OBJSPECTEXT					60005
#define IDC_OBJSETMAT					60006
#define IDC_CLEAROBJECT					60007
#define IDC_OBJECTCONTROL				60008

#define IDR_MAINFRAME					50001
#define IDC_MY3DRENDERER				50002
#define IDS_APP_TITLE					50003
#define IDC_STATIC                      -1

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        101
#define _APS_NEXT_COMMAND_VALUE         40004
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
