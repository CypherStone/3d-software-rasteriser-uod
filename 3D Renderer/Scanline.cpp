#include "Scanline.h"
#include "stdafx.h"

Scanline::Scanline(void)
{

}

Scanline::Scanline(Polygon3D poly)
{
	// The initialisation to finding the height and width for the scanline.
	_yMax = (int)poly.GetVertex(0).y();
	_xMax = (int)poly.GetVertex(0).x();

	for(int i = 0; i < 3; i++)
	{
		if((int)poly.GetVertex(i).y() > _yMax)
			_yMax = (int)poly.GetVertex(i).y();
		else if((int)poly.GetVertex(i).y() < _yMin)
			_yMin = (int)poly.GetVertex(i).y();

		if((int)poly.GetVertex(i).x() > _xMax)
			_xMax = (int)poly.GetVertex(i).x();
		else if((int)poly.GetVertex(i).x() < _xMin)
			_xMin = (int)poly.GetVertex(i).x();
	}

	for (int i = 0; i <= (int)_yMax -_yMin; i++)
		_scanline.push_back(std::vector<int>());
}

Scanline::Scanline(const Scanline &scan)
{
	Copy(scan);
}

Scanline::~Scanline(void)
{
}

void Scanline::AddScanline(int y, int x)
{
	// A simple for loop that does not allow duplicate x values.
	for(int scanx = 0; scanx < (int) _scanline.at(y).size(); scanx++)
	{
		if(_scanline[y][scanx] == x)
			return;
	}
	_scanline.at(y).push_back(x);
}

int Scanline::GetScanlineYSize(int y)
{
	return _scanline.at(y).size();
}

int Scanline::GetScanline(int y, int x)
{
	return _scanline[y][x];
}

int Scanline::GetScanlineMinY(void)
{
	return _yMin;
}

int Scanline::GetScanlineMinX(void)
{
	return _xMin;
}

int Scanline::GetScanlineMaxY(void)
{
	return _yMax;
}

int Scanline::GetScanlineMaxX(void)
{
	return _xMax;
}

void Scanline::Sort(int at)
{
	std::sort(_scanline.at(at).begin(), _scanline.at(at).end());
}

void Scanline::Copy(const Scanline &scan)
{
	_scanline = scan._scanline;
	_yMax = scan._yMax;
	_xMax = scan._xMax;
	_xMin = scan._xMin;
	_yMin = scan._yMin;
}