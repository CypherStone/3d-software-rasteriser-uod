#pragma once
#include "stdafx.h"

class Scanline
{
	public:
		Scanline(void);
		Scanline(Polygon3D poly);
		Scanline(const Scanline &scan);
		~Scanline(void);
		void Sort(int at);

		void AddScanline(int y, int x);
		int GetScanlineYSize(int y);
		int GetScanline(int y, int x);
		int GetScanlineMinY(void);
		int GetScanlineMinX(void);
		int GetScanlineMaxY(void);
		int GetScanlineMaxX(void);


	private:

		std::vector<std::vector<int>> _scanline;
		int _yMax;
		int _yMin;
		int _xMax;
		int _xMin;
		void Copy(const Scanline &scan);
};