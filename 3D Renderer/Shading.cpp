#include "stdafx.h"

COLORREF Shading::ReturnColour(int red, int green, int blue)
{
	// Returns an RGB colour that is in scope.
	ColourAdjust(red);
	ColourAdjust(green);
	ColourAdjust(blue);
	return RGB(red, green, blue);
}

void Shading::ColourAdjust(int &colour)
{
	// Makes sure the colour doesn't go out of scope.
	if (colour > 255)
		colour = 255;
	else if(colour < 0)
		colour = 0;
}

COLORREF Shading::CalculateBlinnPhongLight(Camera camera, LightSource light, Material objMaterial, Vector3D vector, LightingModes shade)
{
	// A material struct to hold the calculated values.
	Material newLight;
	newLight.ambientR = 0.0f;
	newLight.ambientG = 0.0f;
	newLight.ambientB = 0.0f;
	newLight.diffuseR = 0.0f;
	newLight.diffuseG = 0.0f;
	newLight.diffuseB = 0.0f;
	newLight.specularR = 0.0f;
	newLight.specularG = 0.0f;
	newLight.specularB = 0.0f;

	// Normalising just incase it isn't accurate.
	Vector3D normal = vector.Normalize();

	if(shade == AMBIENT || shade == DIFFUSE || shade == SPECULAR)
	{
		// Basic object colour.
		newLight.ambientR = objMaterial.ambientR * light.LightingComponents.ambientR;
		newLight.ambientG = objMaterial.ambientG * light.LightingComponents.ambientG;
		newLight.ambientB = objMaterial.ambientB * light.LightingComponents.ambientB;
	}
	if(shade == DIFFUSE || shade == SPECULAR)
	{
		// Using Lambertian's theory of illumination, I multiply each component.
		float diffuseRadius = max(0.0f, Vector3D::DotProduct(normal, light.Position().Normalize()));
				
		newLight.diffuseR = objMaterial.diffuseR * light.LightingComponents.diffuseR * diffuseRadius;
		newLight.diffuseG = objMaterial.diffuseG * light.LightingComponents.diffuseG * diffuseRadius;
		newLight.diffuseB = objMaterial.diffuseB * light.LightingComponents.diffuseB * diffuseRadius;	
	}

	if(shade == SPECULAR)
	{
		// If the diffusion's are equal to zero do not compute the specular to save un-needed processing.
		if(newLight.diffuseR != 0 && newLight.diffuseG != 0 && newLight.diffuseB != 0)
		{
			// Using the theory behind Blinn's extension to Phong's specularity, I created a half vector and worked out
			// the specularity from that.
			Vector3D halfVector = light.Position() + Vector3D::ToVector(camera.GetPosition());
			const float specDot = max(0.0f, Vector3D::DotProduct(halfVector.Normalize(), normal));
			const float power = pow(specDot, light.SpecularExponent());
			newLight.specularR = objMaterial.specularR * light.LightingComponents.specularR * power;
			newLight.specularG = objMaterial.specularG * light.LightingComponents.specularG * power;
			newLight.specularB = objMaterial.specularB * light.LightingComponents.specularB * power;
		}
	}

	// Calculating each components randiance then multiply 255 by.
	const float radianceR = newLight.ambientR + newLight.diffuseR + newLight.specularR;
	const float radianceG = newLight.ambientG + newLight.diffuseG + newLight.specularG;
	const float radianceB = newLight.ambientB + newLight.diffuseB + newLight.specularB;

	int red =(int)(255 * radianceR);
	int green =(int)(255 * radianceG);
	int blue =(int)(255 * radianceB);

	return ReturnColour(red, green, blue);
}

void Shading::ScanlinePolyFill(HDC &hdc, Scanline scanline, Polygon3D poly)
{
	// A basic scanline for polygon shading.
	HBRUSH brush = CreateSolidBrush(poly.PolygonColour());
	HGDIOBJ oldBrush = SelectObject(hdc, brush);
	for(int y = scanline.GetScanlineMinY(); y <= scanline.GetScanlineMaxY(); y++)
	{	
		int ySum = scanline.GetScanlineMaxY() - y;
		int currentScanlineYSize = scanline.GetScanlineYSize(ySum);

		if(currentScanlineYSize > 0)
		{
			int firstScanPoint = scanline.GetScanline(ySum, 0);
			int lastScanPoint = scanline.GetScanline(ySum, currentScanlineYSize - 1);	

			if(currentScanlineYSize > 1)
			{
				scanline.Sort(ySum);
				firstScanPoint = scanline.GetScanline(ySum, 0);
				lastScanPoint = scanline.GetScanline(ySum, currentScanlineYSize - 1);	
			}	
			BitBlt(hdc, firstScanPoint, y, lastScanPoint - firstScanPoint + 1, 1, hdc, 0, 0, PATCOPY);
		}
	}
	SelectObject(hdc, oldBrush);
	DeleteObject(brush);	
	DeleteObject(oldBrush);
}

void Shading::ScanlineVertexFill(HDC &hdc, Scanline scanline, Polygon3D poly)
{
	// A basic scanline for vertex shading. 
	for(int y = scanline.GetScanlineMinY(); y <= scanline.GetScanlineMaxY(); y++)
	{	
		int ySum = scanline.GetScanlineMaxY() - y;
		int currentScanlineYSize = scanline.GetScanlineYSize(ySum);

		if(currentScanlineYSize > 0)
		{
			int firstScanPoint = scanline.GetScanline(ySum, 0);
			int lastScanPoint = scanline.GetScanline(ySum, currentScanlineYSize - 1);	

			if(currentScanlineYSize > 1)
			{
				scanline.Sort(ySum);
				firstScanPoint = scanline.GetScanline(ySum, 0);
				lastScanPoint = scanline.GetScanline(ySum, currentScanlineYSize - 1);	
			}	

			float start = (float)firstScanPoint;
			float end = (float)lastScanPoint;
			COLORREF first = Geometry::BarycentricInterpolation(Vertex(start, (float)y, 0,0), poly.GetVertex(0), poly.GetVertex(1), poly.GetVertex(2));
			COLORREF second = Geometry::BarycentricInterpolation(Vertex(end, (float)y, 0,0), poly.GetVertex(0), poly.GetVertex(1), poly.GetVertex(2));
			SetPixelV(hdc, (int)start, y, first);
			SetPixelV(hdc, (int)end, y, second);

			for(float position = start + 1; position < end; position++)
			{
				SetPixelV(hdc, (int)position, y, Geometry::Interpolation(position, start, end, first, second));
			}
		}
	}
}

void Shading::ScanlineTextureFill(HDC &hdc, Scanline scanline, Polygon3D poly, HBITMAP texture)
{
	// A scanline fill function for very basic texturing. 
	// It can only handle bitmaps.
	HDC storeDC = CreateCompatibleDC(NULL);
	BITMAP bitmap;
	GetObject(texture, sizeof(BITMAP), &bitmap);
	HBITMAP OldBitmap = (HBITMAP)SelectObject(storeDC, texture);

	for(int y = scanline.GetScanlineMinY(); y <= scanline.GetScanlineMaxY(); y++)
	{	
		int ySum = scanline.GetScanlineMaxY() - y;
		int currentScanlineYSize = scanline.GetScanlineYSize(ySum);

		if(currentScanlineYSize > 0)
		{
			int firstScanPoint = scanline.GetScanline(ySum, 0);
			int lastScanPoint = scanline.GetScanline(ySum, currentScanlineYSize - 1);	

			if(currentScanlineYSize > 1)
			{
				scanline.Sort(ySum);
				firstScanPoint = scanline.GetScanline(ySum, 0);
				lastScanPoint = scanline.GetScanline(ySum, currentScanlineYSize - 1);	
			}	

			for(int position = firstScanPoint; position <= lastScanPoint; position++)
			{
				Vector3D uv = Geometry::BarycentricVector(Vector3D((float)position, (float)y, 0), poly);
				StretchBlt(hdc, position, y, 1, 1, storeDC, bitmap.bmWidth * (int)uv.x(), bitmap.bmHeight * (int)uv.y(), 1, 1, SRCCOPY);
			}
		}
	}
	SelectObject(storeDC, OldBitmap);
	DeleteObject(OldBitmap);
	DeleteDC(storeDC);
}