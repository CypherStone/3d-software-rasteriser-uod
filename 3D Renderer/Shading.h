#pragma once
#include "stdafx.h"

class Shading
{
	public:
		static COLORREF CalculateBlinnPhongLight(Camera camera, class LightSource light, Material mat, Vector3D vector, LightingModes shade);
		static void ColourAdjust (int &colour);
		static COLORREF ReturnColour(int red, int green, int blue);
		static void ScanlinePolyFill(HDC &hdc, Scanline scanline, Polygon3D poly);
		static void ScanlineVertexFill(HDC &hdc, Scanline scanline, Polygon3D poly);
		static void ScanlineTextureFill(HDC &hdc, Scanline scanline, Polygon3D poly, HBITMAP texture);
};