#include "Utility.h"
#include "stdafx.h"

bool Utility::FileDialog(HWND &hWnd, char *fileString, char *title)
{
	// The Windows open file dialog.
	OPENFILENAME fileDialog;
	SecureZeroMemory(&fileDialog, sizeof(fileDialog));
	fileDialog.lStructSize= sizeof(fileDialog);
	fileDialog.hwndOwner = hWnd;
	fileDialog.lpstrTitle = title;
	fileDialog.lpstrFilter = "Object File (*.obj)\0*.obj\0";
	fileDialog.lpstrFile = fileString;
	fileDialog.nMaxFile = MAX_PATH;
	fileDialog.Flags = OFN_EXPLORER | OFN_PATHMUSTEXIST;
	fileDialog.lpstrDefExt = "obj";

	if(GetOpenFileName(&fileDialog))
	{
		return true;
	}

	return false;
}

void Utility::ObjFromFile(Object3D &obj, char *fileString)
{
	// A basic .OBJ file loader with a bitmap loader.
	std::ifstream infile(fileString, std::ifstream::in);

	const int size = 1024;

	// Creating vectors for the vertices, normals and textures.
	std::vector<Vertex> vertex;
	std::vector<Vector3D> normals;
	std::vector<Vector3D> textures;
	bool gotNormals = false;
	bool gotTextures = false;

	while (!infile.eof()) 
	{
		char line[size];
		infile.getline(line, size);
		std::istringstream istring(line, std::istringstream::in);
		char type[size];
		
		istring>>type;

		if(type[0] == 'v' && type[1] == NULL)
		{
			// Vertexes.
			float x, y, z;

			istring>>x>>y>>z;
			vertex.push_back(Vertex(x , y, z, 1.0f));
		}
		else if(type[0] == 'v' && type[1] == 'n')
		{
			// Vertex Normals.
			float x, y, z;

			istring>>x>>y>>z;
			normals.push_back(Vector3D(x , y, z, 1.0f));
			gotNormals = true;
		}
		else if(type[0] == 'v' && type[1] == 't')
		{
			// Vertexes Textures.
			float x, y;

			istring>>x>>y;
			textures.push_back(Vector3D(x , y, 0, 1.0f));
			gotTextures = true;
		}
		else if(type[0] == 'u' && type[1] == 's')
		{
			// Find and retrieve bitmap.
			std::string imageName;
			istring>>imageName;
			std::string file = fileString;

			for(int del = file.size() - 1; del > 0; del--)
			{ 
				if(file.at(del) == '\\')
					break;

				file.pop_back();
			}
			file += imageName;
			HBITMAP loadBitmap = (HBITMAP)LoadImage(NULL, file.c_str(), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE | LR_DEFAULTCOLOR | LR_DEFAULTSIZE);
			if(loadBitmap != NULL)
			{
				obj.SetTexture(loadBitmap);
				obj.SetTextured(true);
			}
		}
		else if(type[0] == 'f')
		{	
			// Polygons.
			int v1, v2, v3;
			int vn1, vn2, vn3;
			int vt1, vt2, vt3;

			std::istringstream newLine(line, std::istringstream::in);
			Polygon3D newPoly;

			if(!gotNormals && !gotTextures)
			{
				istring>>v1>>v2>>v3;

				std::vector<Vertex> vertexNumber;
				vertexNumber.push_back(vertex.at(v1 - 1));
				vertexNumber.push_back(vertex.at(v2 - 1)); 
				vertexNumber.push_back(vertex.at(v3 - 1));
			
				newPoly = Polygon3D(vertexNumber);
			}
			else if(gotNormals && !gotTextures)
			{
				newLine>>type>>v1>>vn1>>v2>>vn2>>v3>>vn3;
				
				std::vector<Vertex> vertexNumber;
				vertexNumber.push_back(vertex.at(v1 - 1));
				vertexNumber.push_back(vertex.at(v2 - 1)); 
				vertexNumber.push_back(vertex.at(v3 - 1));
				
				newPoly = Polygon3D(vertexNumber);
				newPoly.SetNormalVertex(0, normals.at(vn1 - 1).Normalize());
				newPoly.SetNormalVertex(1, normals.at(vn2 - 1).Normalize());
				newPoly.SetNormalVertex(2, normals.at(vn3 - 1).Normalize());
			}
			else if(gotNormals && gotTextures)
			{
				newLine>>type>>v1>>vt1>>vn1>>v2>>vt2>>vn2>>v3>>vt3>>vn3;
				
				std::vector<Vertex> vertexNumber;
				vertexNumber.push_back(vertex.at(v1 - 1));
				vertexNumber.push_back(vertex.at(v2 - 1)); 
				vertexNumber.push_back(vertex.at(v3 - 1));		
				newPoly = Polygon3D(vertexNumber);

				newPoly.SetNormalVertex(0, normals.at(vn1 - 1).Normalize());
				newPoly.SetNormalVertex(1, normals.at(vn2 - 1).Normalize());
				newPoly.SetNormalVertex(2, normals.at(vn3 - 1).Normalize());

				newPoly.SetTexel(0, textures.at(vt1 - 1));
				newPoly.SetTexel(1, textures.at(vt2 - 1));
				newPoly.SetTexel(2, textures.at(vt3 - 1));
			}
			else if(!gotNormals && gotTextures)
			{
				newLine>>type>>v1>>vt1>>v2>>vt2>>v3>>vt3;
				
				std::vector<Vertex> vertexNumber;
				vertexNumber.push_back(vertex.at(v1 - 1));
				vertexNumber.push_back(vertex.at(v2 - 1)); 
				vertexNumber.push_back(vertex.at(v3 - 1));
				newPoly = Polygon3D(vertexNumber);
				
				newPoly.SetTexel(0, textures.at(vt1 - 1));
				newPoly.SetTexel(1, textures.at(vt2 - 1));
				newPoly.SetTexel(2, textures.at(vt3 - 1));
			}
			// Working out the Polygon Normal Vector.
			newPoly.Normal(Vector3D::CrossProduct((Vector3D::ToVector(newPoly.GetVertex(0)) - Vector3D::ToVector(newPoly.GetVertex(2))), (Vector3D::ToVector(newPoly.GetVertex(0)) - Vector3D::ToVector(newPoly.GetVertex(1)))).Normalize());
			// Adds the newly formed polygon to the object.
			obj.AddPolygon(newPoly);
		}
	}

	infile.close();
	
	// Setting the object normals if OBJ file did not contain any or was incorrect to this engine.
	// This is a very expensive operation and will take some time for a high polygon model.
	obj.SetObjectNormals();
}
