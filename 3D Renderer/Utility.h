#pragma once
#include "stdafx.h"
#include "Object3D.h"

class Utility
{
	public:
		static bool FileDialog(HWND &hWnd, char *fileString, char *title);
		static void ObjFromFile(Object3D &obj, char *fileString); 
};