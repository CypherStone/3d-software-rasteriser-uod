#include "Vector3d.h"
#include "stdafx.h"
// Constructors

Vector3D::Vector3D(void)
{
	Init(0, 0,0,1);
}

Vector3D::Vector3D(float x, float y, float z, float w)
{
	Init(x, y, z, w);
}

Vector3D::Vector3D(float x, float y, float z)
{
	Init(x, y, z, 1);
}

Vector3D::Vector3D(const Vector3D& v)
{
	Copy(v);
}

Vector3D::~Vector3D(void)
{
}

// Accessors and mutators
float Vector3D::x(void) const
{
	return _x;
}

void Vector3D::x(float x)
{
	_x = x;
}

float Vector3D::y(void) const
{
	return _y;
}

void Vector3D::y(float y)
{
	_y = y;
}

float Vector3D::z(void) const
{
	return _z;
}

void Vector3D::z(float z)
{
	_z = z;
}

float Vector3D::w(void) const
{
	return _w;
}

void Vector3D::w(float w)
{
	_w = w;
}

void Vector3D::SetLength(void)
{
	float tempX = _x * _x;
	float tempY = _y * _y;
	float tempZ = _z * _z;
	_length = sqrt((tempX + tempY + tempZ)); 
}

float Vector3D::GetLength(void)
{
	return _length;
}

Vector3D Vector3D::Normalize()
{
	SetLength();
	Vector3D temp;
	temp.x(_x / GetLength());
	temp.y(_y / GetLength());
	temp.z(_z / GetLength());
	return temp;
}

// Operator overloads
bool Vector3D::operator==(const Vector3D &rhs)
{
	if(_x == rhs._x && _y == rhs._y && _z == rhs._z)
	{
		return true;
	}
	return false;
}

Vector3D& Vector3D::operator= (const Vector3D &rhs)
{
	if(this != &rhs)
	{
		Copy(rhs);
	}
	return *this;
}

const Vector3D Vector3D::operator+(const Vector3D &other) const
{
	Vector3D temp;
	temp.x(_x + other.x());
	temp.y(_y + other.y());
	temp.z(_z + other.z());
	return temp;
}

Vector3D Vector3D::operator-(const Vector3D &other) const
{
	Vector3D temp;
	temp.x(_x - other.x());
	temp.y(_y - other.y());
	temp.z(_z - other.z());
	return temp;
}

Vector3D Vector3D::operator/(int divider) const
{
	Vector3D temp;
	temp.x(_x / divider);
	temp.y(_y / divider);
	temp.z(_z / divider);
	return temp;
}

Vector3D Vector3D::operator/(const Vector3D &other) const
{
	Vector3D temp;
	temp.x(_x / other.x());
	temp.y(_y / other.y());
	temp.z(_z / other.z());
	return temp;
}

Vector3D Vector3D::operator*(const Vector3D multiply) const
{
	Vector3D temp;
	temp.x(_x * multiply.x());
	temp.y(_y * multiply.y());
	temp.z(_z * multiply.z());
	return temp;
}

Vector3D Vector3D::operator*(float multiply) const
{
	Vector3D temp;
	temp.x(_x * multiply);
	temp.y(_y * multiply);
	temp.z(_z * multiply);
	return temp;
}

Vector3D operator*(const float multiply, const Vector3D &v)
{
	Vector3D temp = v;
	temp.x(multiply * temp.x());
	temp.y(multiply * temp.y());
	temp.z(multiply * temp.z());
	return temp;
}

float Vector3D::DotProduct(Vector3D &a, Vector3D &b)
{
	return (a.x() * b.x()) + (a.y() * b.y()) + (a.z() * b.z());
}

Vector3D Vector3D::CrossProduct(Vector3D &a, Vector3D &b)
{
	float x = 0.0f;
	float y = 0.0f;
	float z = 0.0f;
	float w = 0.0f;
	x = ((a.y() * b.z()) - (a.z() * b.y()));
	y = ((a.z() * b.x()) - (a.x() * b.z()));
	z = ((a.x() * b.y()) - (a.y() * b.x()));
	Vector3D c(x,y,z);
	return c;
}


Vector3D Vector3D::ToVector(Vertex &b)
{
	Vector3D temp;
	temp.x(b.x());
	temp.y(b.y());
	temp.z(b.z());
	return temp;
}

// Private methods
void Vector3D::Init(float x, float y, float z, float w)
{
	_x = x;
	_y = y;
	_z = z;
	_w = w;
}

void Vector3D::Copy(const Vector3D& v)
{
	_x = v.x();
	_y = v.y();
	_z = v.z();
	_w = v.w();
}




