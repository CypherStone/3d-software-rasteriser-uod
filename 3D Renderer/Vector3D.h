#pragma once
#include "stdafx.h"
#include "Vertex.h"

class Vector3D
{
	public:
		Vector3D(void);
		Vector3D(float x, float y, float z);
		Vector3D(float x, float y, float z, float w);
		Vector3D(const Vector3D& v);
		~Vector3D(void);

		float x(void) const;
		void x(float x);
		float y(void) const;
		void y(float y);
		float z(void) const;
		void z(float z);
		float w(void) const;
		void w(float w);

		void SetLength(void);
		float GetLength(void);
		Vector3D Normalize();
		bool operator==(const Vector3D &rhs);
		Vector3D& operator= (const Vector3D &rhs);
		const Vector3D operator+(const Vector3D &other) const;
		Vector3D operator-(const Vector3D &other) const;
		Vector3D operator/(int divider) const;
		Vector3D operator/(const Vector3D &other) const;
		Vector3D operator*(const Vector3D multiply) const;
		Vector3D operator*(float multiply) const;
		Vector3D friend operator*(const float multiply, const Vector3D &v);

		static float DotProduct(Vector3D &a, Vector3D &b);
		static Vector3D CrossProduct(Vector3D &a, Vector3D &b);
		static Vector3D ToVector(class Vertex &b);

	private:
		float _x;
		float _y;
		float _z;
		float _w;
		float _length;

		void Init(float _x, float _y, float _z, float _w);
		void Copy(const Vector3D& v);
		
};
