#include "Vertex.h"
#include "stdafx.h"

Vertex::Vertex(void)
{
	Init(0, 0, 0, 1);
}

Vertex::Vertex(float x, float y, float z,  float w)
{
	Init(x, y, z, w);
}

Vertex::Vertex(const Vertex& v)
{
	Copy(v);
}

Vertex::~Vertex(void)
{
}

float Vertex::x(void) const
{
	return _vector.x();
}

void Vertex::x(float x)
{
	_vector.x(x);
}

float Vertex::y(void) const
{
	return _vector.y();
}

void Vertex::y(float y)
{
	_vector.y(y);
}

float Vertex::z(void) const
{
	return _vector.z();
}

void Vertex::z(float z)
{
	_vector.z(z);
}

float Vertex::w(void) const
{
	return _vector.w();
}

void Vertex::w(float w)
{
	_vector.w(w);
}

COLORREF Vertex::Colour(void)
{
	return _colour;
}

void Vertex::Colour(COLORREF col)
{
	_colour = col;
}

Vertex& Vertex::operator= (const Vertex &rhs)
{
	Copy(rhs);
	return *this;
}

Vertex& Vertex::operator= (const Vector3D &rhs)
{
	Copy(rhs);
	return *this;
}

// Private methods
void Vertex::Init(float x, float y, float z, float w)
{
	_vector.x(x);
	_vector.y(y);
	_vector.z(z);
	_vector.w(w);
	_vector.SetLength();
	_colour = RGB(0,0,0);
}

void Vertex::Copy(const Vertex &v)
{
	_vector.x(v.x());
	_vector.y(v.y());
	_vector.z(v.z());
	_vector.w(v.w());
	_colour = v._colour;
}

void Vertex::Copy(const Vector3D &v)
{
	_vector.x(v.x());
	_vector.y(v.y());
	_vector.z(v.z());
	_vector.w(v.w());
}