#pragma once
#include "stdafx.h"
#include "Vector3d.h"

class Vertex
{
	public:
		Vertex(void);
		Vertex(float x, float y, float z, float w);
		Vertex(const Vertex &v);
		~Vertex(void);

		float x(void) const;
		void x(float x);
		float y(void) const;
		void y(float y);
		float z(void) const;
		void z(float z);
		float w(void) const;
		void w(float w);

		COLORREF Colour();
		void Colour(COLORREF col);
		Vertex& operator= (const Vertex &rhs);
		Vertex& operator= (const Vector3D &rhs);
		const Vertex operator+(const Vertex &other) const;
		const Vertex operator-(const Vertex &other) const;

	private:
		Vector3D _vector;
		COLORREF _colour;
		void Init(float x, float y, float z, float w);
		void Copy(const Vertex &v);
		void Copy(const Vector3D &v);
};


