// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//
#pragma once

#include "targetver.h"

//#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers
// Windows Header Files:
#include <windows.h>
#include <CommDlg.h>
#include <commctrl.h>

// C RunTime Header Files
#include <stdlib.h>
#include <malloc.h>
#include <memory.h>
#include <tchar.h>
#include <vector>
#include <cmath>
#include <iostream>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <string>
#include <ppl.h>
#include <concurrent_vector.h>

// TODO: reference additional headers your program requires here
#include "Polygon.h"
#include "Vector3d.h"
#include "Vertex.h"
#include "Matrix.h"
#include "Scanline.h"
#include "Camera.h"
#include "Object3D.h"
#include "LightSource.h"
#include "3D Renderer.h"
#include "RenderEngine.h"
#include "Mouse.h"
#include "Resource.h"
#include "Shading.h"
#include "Geometry.h"
#include "Utility.h"


